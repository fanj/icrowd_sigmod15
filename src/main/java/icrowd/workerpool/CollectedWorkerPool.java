package icrowd.workerpool;

import java.util.*;

public class CollectedWorkerPool implements WorkerPool {
	List<String> mWorkers; // the sequence of worker requests
	int cur = 0; // the index of requesting worker
	Map<String, Map<String, Double>> w2t2a; // worker --> task --> answer 
	
	public CollectedWorkerPool (List<String> workers, 
			Map<String, Map<String, Double>> worker2task2answer) {
		mWorkers = workers;
		w2t2a = worker2task2answer;
	}
	
	public String getRequestingWorker() {
		if (cur >= mWorkers.size()) return null;
		String worker = mWorkers.get(cur);
		cur ++;
		return worker;
	}

	
	public double getWorkerAnswer(String task, String worker) {
		Map<String, Double> task2answer = w2t2a.get(worker);
		return task2answer.get(task);
	}
	
	public Map<String, List<String>> getWorker2CandidateTasks () {
		Map<String, List<String>> worker2cands = 
				new HashMap<String, List<String>> ();
		for (String worker : this.w2t2a.keySet()) {
			List<String> cands = new ArrayList<String> ();
			for (String task : w2t2a.get(worker).keySet()) {
				cands.add(task);
			}
			worker2cands.put(worker, cands);
		}
		return worker2cands;
	}
}
