package icrowd.utils;

import java.util.*;

public class ScoredItem implements Comparable<Object>{
	Comparable mObj;
	double mScore;
	
	public ScoredItem (Comparable obj, double score) {
		mObj = obj;
		mScore = score;
	}
	
	public Comparable getObj () {
		return mObj;
	}
	
	public double getScore (){
		return mScore;
	}

	public int compareTo(Object arg0) {
		ScoredItem a = (ScoredItem) arg0;
		if (mObj.equals(a.mObj)) {
			return 0;
		}
		if (mScore > a.mScore){
			return -1;
		} else if (mScore < a.mScore) {
			return 1;
		} else {
			if (mObj.compareTo(a.mObj) > 0) {
				return -1;
			} else if (mObj.compareTo(a.mObj) < 0) {
				return 1;
			} else {
				return 0;
			}
		}
	}
	
	public String toString () {
		return mObj.toString() + "(" + String.format("%.3f", mScore) + ")";
	}
	
	public boolean equals (Object arg0) {
		ScoredItem a = (ScoredItem) arg0;
		return this.mObj.equals(a.mObj);
	}
	
	public int hashCode () {
		return this.mObj.hashCode();
	}
	
	public static void main (String args[]) throws Exception {
		ScoredItem t1 = new ScoredItem("WORKER-2", 0.5);
		ScoredItem t2 = new ScoredItem("WORKER-4", 0.5);
		
		String s1 = "A";
		String s2 = "B";
		
		System.out.println (t1.compareTo(t2));
		
		System.out.println(s1.compareTo(s2));
		TreeSet<ScoredItem> set = new TreeSet<ScoredItem> ();
		set.add(t1);
		set.add(t2);
		
		ScoredItem q = new ScoredItem("WORKER-6", 0.1);
		System.out.println(set.contains(q));
	}
	
}












