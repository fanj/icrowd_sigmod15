package icrowd.utils;

import java.util.*;

public class SortedList {
	List<String> elements;
	Map<String, Double> scores;
	int k;
	
	public SortedList (int topK) {
		elements = new ArrayList<String> ();
		scores = new HashMap<String, Double> ();
		k = topK;
	}
	
	/**
	 * Update the list 
	 * @param element
	 * @param score
	 * @return
	 */
	public Map<String, Boolean> update (String element, Double score) {
		if (elements.contains(element)) {
			// TODO: Refine the `contain` function
			elements.remove(element); // as the index has been invalid
		} 
		scores.put(element, score);
		Map<String, Boolean> shift = doBinaryInsert (element, score);
		return shift;
	}

	/**
	 * The basic structure for binary search
	 * @param element: the element to be inserted
	 * @param score: the score of the inserted element
	 * @return: the upgrade / downgrade of top-k
	 */
	private Map<String, Boolean> doBinaryInsert(String element, Double score) {
		Map<String, Boolean> upgrade = new HashMap<String, Boolean> ();
		int oldIndex = elements.indexOf(element);
		// remove the original ones
		// To maintain the order, do a binary-search insert
		int start = 0;
		int end = elements.size() - 1;
		while (start <= end) {
			int center = (start + end) / 2;
			String centerEle = elements.get(center);
			double centerScore = scores.get(centerEle);
			if (centerScore > score) { // worker should be moved back
				start = center + 1;
			} else if (centerScore < score){ // worker should be moved forward
				end = center - 1;
			} else {
				if (centerEle.compareTo(element) > 0) {
					start = center + 1;
				} else {
					end = center - 1;
				}
			}
		}
		elements.add(start, element);
		if (oldIndex == -1) { // similar to insert
			if (start < k) { // The top-k has been changed
				upgrade.put(element, true); // `worker` upgraded
				if (elements.size() > k) {
					upgrade.put(elements.get(k), false);
					// `worker` at position k down-graded. 
				}
			}
		} else {
			if (oldIndex >= k && start < k) { // worker is moved into top-k
				upgrade.put(element, true); // `worker` upgraded
				upgrade.put(elements.get(k), false);
				// `worker` at position k down-graded. 
			} else if (oldIndex < k && start >= k) { // worker is moved out of top-k
				upgrade.put(element, false); // `worker` down-grade
				upgrade.put(elements.get(k), true);
				// `worker` at position k upgraded. 
			}
		}
		return upgrade;		
	}

	public int length() {
		return elements.size();
	}

	public String getElement(int i) {
		return elements.get(i);
	}

	public Double getScore(int i) {
		String element = this.getElement(i);
		return scores.get(element);
	}

	public void remove(String element) {
		elements.remove(element);
		scores.remove(element);
	}

	public void clear() {
		elements.clear();
		scores.clear();
	}
	
	public String toString () {
		StringBuffer buffer = new StringBuffer ();
		for (int i = 0; i < elements.size(); i ++) {
			String element = elements.get(i);
			double score = scores.get(element);
			buffer.append(element + " (" + score + "), ");
		}
		return buffer.toString();
	}

}
