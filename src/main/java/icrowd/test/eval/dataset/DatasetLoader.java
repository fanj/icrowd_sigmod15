package icrowd.test.eval.dataset;

import icrowd.core.AsgMatrix;
import icrowd.utils.*;

import java.io.*;
import java.util.*;

import org.json.JSONArray;
import org.json.JSONObject;


public class DatasetLoader {
	static DatasetLoader aux;
	
	public static final String DS_CONFIG_FILE = "src/main/resources/ds-config.json";
	
	public static DatasetLoader init (String dsName) throws Exception {
		String dsconfig = FileUtil.file2String(DS_CONFIG_FILE);
		JSONArray jdatasets = new JSONArray (dsconfig);
		for (int i = 0; i < jdatasets.length(); i ++) {
			JSONObject jdataset = jdatasets.getJSONObject(i);
			if (jdataset.getString("name").equals(dsName)) {
				aux = new DatasetLoader ();
				aux.ppvFilename = jdataset.getString("ppvfile");
				aux.qualiFilename = jdataset.getString("qualification-file");
				// Step 1: Load the tasks
				String taskfile = jdataset.getString("taskfile");
				aux.tasks = new ArrayList<String> ();
				BufferedReader r = new BufferedReader (new FileReader (taskfile));
				String line = r.readLine();
				while (line != null) {
					line = line.trim();
					if (!line.isEmpty()) {
						aux.tasks.add(line);
					}
					line = r.readLine();
				}
				r.close();
				
				// Step 2: Load the worker requests
				String workerfile = jdataset.getString("workerfile");
				aux.workers = new ArrayList<String> ();
				r = new BufferedReader (new FileReader (workerfile));
				line = r.readLine();
				while (line != null) {
					line = line.trim();
					if (!line.isEmpty()) {
						aux.workers.add(line);
					}
					line = r.readLine();
				}
				r.close();
				
				// Step 3: Load task categories (unknown to algorithms and only for evaluation purpose)
				String catfile = jdataset.getString("categoryfile");
				aux.task2cat = new HashMap<String, String>();
				
				r = new BufferedReader(new FileReader(catfile));
				line = r.readLine();
				while (line != null) {
					line = line.trim();
					String tmps[] = line.split("\t");
					if (tmps.length == 2) {
						String task = tmps[0];
						String cat = tmps[1];
						aux.task2cat.put(task, cat);
					}
					line = r.readLine();
				}
				r.close();
				
				// Step 4: Load the gold answers of workers for tasks
				String goldfile = jdataset.getString("goldfile");
				aux.gold = new AsgMatrix ();
				r = new BufferedReader (new FileReader (goldfile));
				line = r.readLine();
				while (line != null) {
					String parts[] = line.split("\t");
					if (parts.length == 3) {
						String task = parts[0];
						String worker = parts[1];
						double benefit = Double.parseDouble(parts[2]);
						aux.gold.put(task, worker, benefit);
					}
					line = r.readLine();
				}
				r.close();
				
				break;
			}
		}
		return aux;
	}

	public static DatasetLoader getIns() {
		return aux;
	}

	Map<String, String> task2cat;
	private AsgMatrix gold;
	List<String> workers;
	List<String> tasks;
	String folder;
	String qualiFilename;
	
	String ppvFilename;

	public double getGoldAnswer (String worker, String task) throws Exception {
		Double ans = gold.getBenefit(task, worker);
		if (ans == null) throw new Exception ("Not valid assignment");
		return ans;
	}
	
	public String getPPVFilename() {
		return ppvFilename;
	}

	
	public Map<String, Double> getGoldAnswers (String worker) {
		return gold.getAssignedTaskScores(worker);
	}
	
	public String getCategory(String task) {
		return task2cat.get(task);
	}
	
	public Map<String, String> getTaskCategory () {
		return task2cat;
	}

	public List<String> getTasks() {
		return tasks;
	}
	
	public String getQualificationFileName () {
		return this.qualiFilename;
	}

	public List<String> getWorkerSequence() {
		return workers;
	}

	public Map<String, Map<String, Double>> getGoldAnswers() {
		return gold.worker2tasks;
	}

	public String getAuxFolder() {
		return folder;
	}
	
	public List<String> getCandidateTasks (String worker) {
		return gold.getAssignedTasks(worker);
	}
	
	public Map<String, List<String>> getWorkerCandidateTasks () {
		Map<String, List<String>> workerCandtasks = new HashMap<String, List<String>> ();
		for (String worker : gold.worker2tasks.keySet()) {
			workerCandtasks.put(worker, gold.getAssignedTasks(worker));
		}
		return workerCandtasks;
	}

	private void showDomainAccuracies() {
		Set<String> cats = new TreeSet<String> ();
		Map<String, Map<String, Double>> worker2cat2acc = 
				new HashMap<String, Map<String, Double>> ();
		Map<String, Map<String, Double>> worker2cat2num = 
				new HashMap<String, Map<String, Double>> ();
		List<ScoredItem> sortedWorkers = new ArrayList<ScoredItem> ();
		Map<String, Set<String>> task2workers = 
				new HashMap<String, Set<String>> ();
		for (String worker : gold.worker2tasks.keySet()) {
			sortedWorkers.add(new ScoredItem (worker, 
					gold.worker2tasks.get(worker).size()));
			Map<String, Double> cat2cor = new HashMap<String, Double> ();
			Map<String, Double> cat2num = new HashMap<String, Double> ();
			for (String task : gold.worker2tasks.get(worker).keySet()) {
				Set<String> workers = task2workers.get(task);
				if (workers == null) workers = new HashSet<String> ();
				workers.add(worker);
				task2workers.put(task, workers);
				
				String cat = task2cat.get(task);
				cats.add(cat);
				double s = gold.worker2tasks.get(worker).get(task);
				Double num = cat2num.get(cat);
				if (num == null) num = 0.0;
				num ++;
				cat2num.put(cat, num);
				Double score = cat2cor.get(cat);
				if (score == null) score = 0.0;
				score += s;
				cat2cor.put(cat, score);
			}
			Map<String, Double> cat2acc = new HashMap<String, Double> ();
			for (String cat : cat2num.keySet()) {
				Double num = cat2num.get(cat);
				Double cor = cat2cor.get(cat);
				if (cor == null) cor = 0.0;
				cat2acc.put(cat, cor / num);
			}
			worker2cat2acc.put(worker, cat2acc);
			worker2cat2num.put(worker, cat2num);
		}
		Collections.sort(sortedWorkers);
		
		System.out.print ("Worker\tTaskNum\t");
		for (String cat : cats) System.out.print(cat + "\t");
		System.out.println();
		
		for (ScoredItem item : sortedWorkers) {
			String worker = (String)item.getObj();
			double occ = item.getScore();
			Map<String, Double> cat2acc = worker2cat2acc.get(worker);
			System.out.print (worker + "\t" + occ + "\t");
			for (String cat : cats) {
				Double acc = cat2acc.get(cat);
				if (acc == null) System.out.print ("-\t");
				else System.out.print ( acc + "\t");
			}
			System.out.println();
		}
		
		int topk = 3;
		System.out.println ("Print average accuracy of top-" + 
				topk + " workers of each task");
		
		for (String task : this.tasks) {
			String cat = this.getCategory(task);
			List<Double> accs = new ArrayList<Double> ();
			Set<String> workers = task2workers.get(task);
			System.out.print (task + "\t");
			for (String worker : workers) {
				Map<String, Double> cat2acc = worker2cat2acc.get(worker);
				double acc = cat2acc.get(cat);
				Map<String, Double> cat2num = worker2cat2num.get(worker);
				Double num = cat2num.get(cat);
				if (num != null && num >= 10)
					accs.add(acc);
			}
			Collections.sort(accs);
			Collections.reverse(accs);
			for (double acc : accs) System.out.print (acc + "\t");
			System.out.println();
		}
		
	}
	
	public static void main (String arg[]) throws Exception {
		DatasetLoader.init("itemcompare");
		DatasetLoader taskLoader = DatasetLoader.getIns();
		
		//TaskLoader.showWorkerOccurrence ();
		taskLoader.showDomainAccuracies ();
	}
}
