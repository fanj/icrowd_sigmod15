package icrowd.test.eval.dataset;

import java.util.*;

public class Simulator extends DatasetLoader {
	public static Integer TASK_NUM = null;
	public static Integer WORKER_NUM = null;
	public static Integer WORKER_ROUND = null;
	public static Integer PPV_SIZE = null;
	
	public Simulator () {
		tasks = new ArrayList<String> ();
		for (int i = 0; i < TASK_NUM; i ++) {
			tasks.add("task" + String.valueOf(i));
		}
		
		this.workers = new ArrayList<String> ();
		for (int i = 0; i < WORKER_ROUND; i ++) {
			Random random = new Random();
			int pos = random.nextInt(WORKER_NUM);
			workers.add("worker_" + pos);
		}
	}
	
	public String getCategory(String task) {
		return "simu";
	}
	
	public List<String> getCandidateTasks (String worker) {
		return null;
	}
	
	public Map<String, Double> getGoldAnswers (String worker) {
		Map<String, Double> goldans = new HashMap<String, Double> ();
		for (String task : tasks) {
			double ans = this.getGoldAnswer(worker, task);
			goldans.put(worker, ans);
		}
		return goldans;
	}
	
	public double getGoldAnswer (String worker, String task) {
		Random random = new Random();
		int answer = random.nextInt(2);
		return answer; // generate random answers for simulation
	}

	public Map<String, Double> simulatePPV() {
		Map<String, Double> ppv = new HashMap<String, Double> ();
		for (int i = 0; i < PPV_SIZE; i ++) {
			Random random = new Random();
			int randomIndex = random.nextInt(tasks.size());
			double rnd = Math.random();
			String task = tasks.get(randomIndex);
			ppv.put(task, rnd);
		}
		
		return ppv;
	}
}
