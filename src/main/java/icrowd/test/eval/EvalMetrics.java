package icrowd.test.eval;

import icrowd.core.AsgMatrix;
import icrowd.test.eval.dataset.DatasetLoader;
import icrowd.utils.ScoredItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class EvalMetrics {
	DatasetLoader dataset;
	public EvalMetrics () throws Exception{
		dataset = DatasetLoader.getIns();
	}
	
	public double evaluteAccuracy(List<String> allTasks, Map<String, Integer> task2result) {
		System.out.println("# of finished tasks: " + task2result.size());
		Map<String, Double> cat2pos = new HashMap<String, Double> ();
		Map<String, Integer> cat2num = new HashMap<String, Integer> ();
		Map<String, Integer> cat2cmpnum = new HashMap<String, Integer> ();
		double posNum = 0.0;
		double totalNum = 0.0;
		for (String task : allTasks) {
			String cat = dataset.getCategory(task);
			Integer num = cat2num.get(cat);
			if (num == null) num = 0;
			num ++;
			cat2num.put(cat, num);
			totalNum ++;
			
			if (!task2result.containsKey(task)) {
				//System.out.println ("Incompleted: " + task);
				continue;
			}
			
			Integer cmpnum = cat2cmpnum.get(cat);
			if (cmpnum == null) cmpnum = 0;
			cmpnum ++;
			cat2cmpnum.put(cat, cmpnum);
			
			int decision = task2result.get(task);
			if (decision == 1) {
				Double pos = cat2pos.get(cat);
				if (pos == null) pos = 0.0;
				pos ++;
				cat2pos.put(cat, pos);
				posNum ++;
			}
			
		}
		
		double overallAcc = posNum / totalNum;
		System.out.println ("############### Accuracy Evaluation ###############");
		System.out.println("Task Category\t" + "Task Number\t" + "Accuracy");
		System.out.println("Overall\t" + totalNum + "\t" + (overallAcc)  );
		for (String cat : cat2num.keySet()) {
			Double pos = cat2pos.get(cat);
			if (pos == null) pos = 0.0;
			int num = cat2num.get(cat);
			int cmpnum = cat2cmpnum.get(cat);
			System.out.println (cat + "\t" + cmpnum + "/" + num + "\t" + (pos / num));
		}
		System.out.println ("###################################################");
		return overallAcc;
	}

	public void evaluteAssignment(AsgMatrix asg,  Map<String, Integer> task2result) throws Exception {
		Map<String, Map<String, Double>> cat2worker2num = 
				new HashMap<String, Map<String, Double>>(); 
		Map<String, Map<String, Double>> cat2worker2cor = 
				new HashMap<String, Map<String, Double>>();
		
		Map<String, Map<String, Set<String>>> cat2task2workers = 
				new HashMap<String, Map<String, Set<String>>> ();
		
		List<ScoredItem> workernum = new ArrayList<ScoredItem> ();
		
		for (String worker : asg.worker2tasks.keySet()) {
			workernum.add(new ScoredItem(worker, asg.worker2tasks.get(worker).size()));
			Map<String, Double> task2acc = asg.worker2tasks.get(worker);
			for (String task : task2acc.keySet()) {
				String cat = dataset.getCategory(task);
				Map<String, Set<String>> task2workers = cat2task2workers.get(cat);
				if (task2workers == null) task2workers = new HashMap<String, Set<String>> ();
				Set<String> workers = task2workers.get(task);
				if (workers == null) workers = new HashSet<String> ();
				workers.add(worker);
				task2workers.put(task, workers);
				cat2task2workers.put(cat, task2workers);
				
				double acc = task2acc.get(task);
				Map<String, Double> worker2num = cat2worker2num.get(cat);
				if (worker2num == null) worker2num = new HashMap<String, Double> ();
				Double num = worker2num.get(worker);
				if (num == null) num = 0.0;
				num ++;
				worker2num.put(worker, num);
				cat2worker2num.put(cat, worker2num);
				
				if (acc == 1.0) {
					Map<String, Double> worker2cor = cat2worker2cor.get(cat);
					if (worker2cor == null) worker2cor = new HashMap<String, Double> ();
					Double cor = worker2cor.get(worker);
					if (cor == null) cor = 0.0;
					cor ++;
					worker2cor.put(worker, cor);
					cat2worker2cor.put(cat, worker2cor);
				} else {
				}
			}
		}
		
		for (String cat : cat2task2workers.keySet()) {
			Map<String, Set<String>> task2workers = cat2task2workers.get(cat);
			//double meanavgacc = 0.0;
			//double meanaggacc = 0.0;
			
			for (String task : task2workers.keySet()) {
				//System.out.print(task2result.get(task) + "\t" + task + "\t");
				Set<String> workers = task2workers.get(task);
				//double avgacc = 0.0;
				for (String worker : workers) {
					double num = cat2worker2num.get(cat).get(worker);
					Map<String, Double> worker2cor = cat2worker2cor.get(cat);
					double cor = 0.0;
					if (worker2cor != null && worker2cor.containsKey(worker)) 
						cor = worker2cor.get(worker);
					//System.out.print((cor / num) + "\t");
					//avgacc += cor / num;
				}
				//System.out.println();
				//avgacc /= workers.size();
				//meanavgacc += avgacc;
			}
			//meanavgacc /= task2workers.size();
			//System.out.println(cat + "\t" + meanavgacc);
			System.out.println ();
//			List<ScoredItem> sorted = new ArrayList<ScoredItem> ();
//			Map<String, Double> worker2num = cat2worker2num.get(cat);
//			Map<String, Double> worker2cor = cat2worker2cor.get(cat);
//			for (String worker : worker2num.keySet()) {
//				sorted.add(new ScoredItem (worker, worker2num.get(worker)));
//			}
//			Collections.sort(sorted);
//			System.out.println("Category: " + cat);
//			for (int i = 0; i < sorted.size() && i < 10; i ++) {
//				ScoredItem item = sorted.get(i);
//				String worker = (String) item.getObj();
//				double num = item.getScore();
//				Double cor = cat2worker2cor.get(cat).get(worker);
//				if (cor == null) cor = 0.0;
//				System.out.println("\t" + worker + "\t" + num + "\t" + (cor / num) );
//			}
//			Map<String, Set<String>> task2workers = cat2task2workers.get(cat);
//			Histogram hist = new Histogram (0, 1, 10);
//			for (String task : task2workers.keySet()) {
//				Set<String> workers = task2workers.get(task);
//				double avg = 0.0;
//				for (String worker : workers) {
//					Double cor = worker2cor.get(worker);
//					if (cor == null) cor = 0.0;
//					double num = worker2num.get(worker);
//					avg += cor / num;
//				}
//				avg /= workers.size();
//				hist.addValue(avg, 1.0);
//			}
//			//System.out.println(hist);
		}
	}
	
	
	/*
	public double evaluteAccuracy(List<String> allTasks, Map<String, Integer> task2result) {
		System.out.println("# of finished tasks: " + task2result.size());
		Map<String, Double> cat2pos = new HashMap<String, Double> ();
		Map<String, Integer> cat2num = new HashMap<String, Integer> ();
		Map<String, Integer> cat2cmpnum = new HashMap<String, Integer> ();
		double posNum = 0.0;
		double totalNum = 0.0;
		for (String task : allTasks) {
			String cat = taskAux.getCategory(task);
			Integer num = cat2num.get(cat);
			if (num == null) num = 0;
			num ++;
			cat2num.put(cat, num);
			totalNum ++;
			
			if (!task2result.containsKey(task)) {
				//System.out.println ("Incompleted: " + task);
				continue;
			}
			
			Integer cmpnum = cat2cmpnum.get(cat);
			if (cmpnum == null) cmpnum = 0;
			cmpnum ++;
			cat2cmpnum.put(cat, cmpnum);
			
			int decision = task2result.get(task);
			if (decision == 1) {
				Double pos = cat2pos.get(cat);
				if (pos == null) pos = 0.0;
				pos ++;
				cat2pos.put(cat, pos);
				posNum ++;
			}
			
		}
		
		double overallAcc = posNum / totalNum;
		System.out.println("Overall\t" + totalNum + "\t" + (overallAcc)  );
		for (String cat : cat2num.keySet()) {
			Double pos = cat2pos.get(cat);
			if (pos == null) pos = 0.0;
			int num = cat2num.get(cat);
			int cmpnum = cat2cmpnum.get(cat);
			System.out.println (cat + "\t" + cmpnum + "/" + num + "\t" + (pos / num));
		}
		return overallAcc;
	} */
/*
	public void evaluteAssignment(AsgMatrix asg,  Map<String, Integer> task2result) throws Exception {
		Map<String, Map<String, Double>> cat2worker2num = 
				new HashMap<String, Map<String, Double>>(); 
		Map<String, Map<String, Double>> cat2worker2cor = 
				new HashMap<String, Map<String, Double>>();
		
		Map<String, Map<String, Set<String>>> cat2task2workers = 
				new HashMap<String, Map<String, Set<String>>> ();
		
		List<ScoredItem> workernum = new ArrayList<ScoredItem> ();
		
		for (String worker : asg.worker2tasks.keySet()) {
			workernum.add(new ScoredItem(worker, asg.worker2tasks.get(worker).size()));
			Map<String, Double> task2acc = asg.worker2tasks.get(worker);
			for (String task : task2acc.keySet()) {
				String cat = taskAux.getCategory(task);
				Map<String, Set<String>> task2workers = cat2task2workers.get(cat);
				if (task2workers == null) task2workers = new HashMap<String, Set<String>> ();
				Set<String> workers = task2workers.get(task);
				if (workers == null) workers = new HashSet<String> ();
				workers.add(worker);
				task2workers.put(task, workers);
				cat2task2workers.put(cat, task2workers);
				
				double acc = task2acc.get(task);
				Map<String, Double> worker2num = cat2worker2num.get(cat);
				if (worker2num == null) worker2num = new HashMap<String, Double> ();
				Double num = worker2num.get(worker);
				if (num == null) num = 0.0;
				num ++;
				worker2num.put(worker, num);
				cat2worker2num.put(cat, worker2num);
				
				if (acc == 1.0) {
					Map<String, Double> worker2cor = cat2worker2cor.get(cat);
					if (worker2cor == null) worker2cor = new HashMap<String, Double> ();
					Double cor = worker2cor.get(worker);
					if (cor == null) cor = 0.0;
					cor ++;
					worker2cor.put(worker, cor);
					cat2worker2cor.put(cat, worker2cor);
				} else {
				}
			}
		}
		
		for (String cat : cat2task2workers.keySet()) {
			Map<String, Set<String>> task2workers = cat2task2workers.get(cat);
			double meanavgacc = 0.0;
			double meanaggacc = 0.0;
			
			for (String task : task2workers.keySet()) {
				//System.out.print(task2result.get(task) + "\t" + task + "\t");
				Set<String> workers = task2workers.get(task);
				double avgacc = 0.0;
				for (String worker : workers) {
					double num = cat2worker2num.get(cat).get(worker);
					Map<String, Double> worker2cor = cat2worker2cor.get(cat);
					double cor = 0.0;
					if (worker2cor != null && worker2cor.containsKey(worker)) 
						cor = worker2cor.get(worker);
					//System.out.print((cor / num) + "\t");
					avgacc += cor / num;
				}
				//System.out.println();
				avgacc /= workers.size();
				meanavgacc += avgacc;
			}
			meanavgacc /= task2workers.size();
			//System.out.println(cat + "\t" + meanavgacc);
			System.out.println ();
//			List<ScoredItem> sorted = new ArrayList<ScoredItem> ();
//			Map<String, Double> worker2num = cat2worker2num.get(cat);
//			Map<String, Double> worker2cor = cat2worker2cor.get(cat);
//			for (String worker : worker2num.keySet()) {
//				sorted.add(new ScoredItem (worker, worker2num.get(worker)));
//			}
//			Collections.sort(sorted);
//			System.out.println("Category: " + cat);
//			for (int i = 0; i < sorted.size() && i < 10; i ++) {
//				ScoredItem item = sorted.get(i);
//				String worker = (String) item.getObj();
//				double num = item.getScore();
//				Double cor = cat2worker2cor.get(cat).get(worker);
//				if (cor == null) cor = 0.0;
//				System.out.println("\t" + worker + "\t" + num + "\t" + (cor / num) );
//			}
//			Map<String, Set<String>> task2workers = cat2task2workers.get(cat);
//			Histogram hist = new Histogram (0, 1, 10);
//			for (String task : task2workers.keySet()) {
//				Set<String> workers = task2workers.get(task);
//				double avg = 0.0;
//				for (String worker : workers) {
//					Double cor = worker2cor.get(worker);
//					if (cor == null) cor = 0.0;
//					double num = worker2num.get(worker);
//					avg += cor / num;
//				}
//				avg /= workers.size();
//				hist.addValue(avg, 1.0);
//			}
//			//System.out.println(hist);
		}
	}
	*/
	
}
