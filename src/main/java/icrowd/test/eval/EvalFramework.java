package icrowd.test.eval;

import icrowd.core.AsgMatrix;
import icrowd.core.CrowdFramework;
import icrowd.core.aggregator.*;
import icrowd.core.assigner.*;
import icrowd.core.warmup.*;
import icrowd.test.eval.dataset.DatasetLoader;
import icrowd.workerpool.CollectedWorkerPool;

import java.io.*;
import java.util.*;

/**
 * This class is the framework for evaluating: 1) task assignment (given a set
 * of tasks and a sequence of worker requests) 2) crowdsourcing answer
 * aggregation (given worker answers).
 * 
 * This evaluation framework is based on the collected answers.
 * 
 * @author Ju Fan
 *
 */
public class EvalFramework {

	public static void main(String args[]) throws Exception {
		if (args.length != 5) {
			System.err
					.println("Usage: EvalFramework dataset out-folder assign-size assigner aggregator\n"
							+ "Example: EvalFramework sample ./out/adaptive 3 AdaptiveAssign ProbVote");
			return;
		}
		String datasetName = args[0];
		String outfolder = args[1];
		int assignSize = Integer.parseInt(args[2]);
		String assignerName = args[3];
		String aggregatorName = args[4];

		DatasetLoader dataset = DatasetLoader.init(datasetName);
		if (dataset == null) {
			System.err.println("No dataset specified");
			return;
		}

		CollectedWorkerPool cwp = new CollectedWorkerPool(
				dataset.getWorkerSequence(), dataset.getGoldAnswers());

		CrowdFramework crowdFramework = new CrowdFramework(dataset.getTasks(),
				assignSize, cwp.getWorker2CandidateTasks());

		TaskAssigner assigner = buildTaskAssigner(assignerName, dataset);
		AnswerAggregator aggregator = buildAnswerAggregator(aggregatorName);
		WarmUp warmup = new WarmupQualification (dataset.getQualificationFileName());

		crowdFramework.initTaskAssigner(assigner);
		crowdFramework.initAnswerAggregator(aggregator);
		crowdFramework.initWarmUp(warmup);

		System.out.println("Start to evaluate the algorithms: " + assignerName
				+ ", " + aggregatorName);
		double latency = 0; // the overall latency
		int asgnum = 0; // the number of assignment

		Set<String> underqualified = new HashSet<String>();
		// the set containing workers cannot pass qualification test 
		
		String worker = cwp.getRequestingWorker();
		while (worker != null) {
			if (!underqualified.contains(worker)) {
				long t1 = System.currentTimeMillis();
				List<String> qtasks = null;
				if (!crowdFramework.containsWorker(worker)) {
					qtasks = crowdFramework.assignQTasks(worker);
					List<String> validqtasks = new ArrayList<String> ();
					List<Double> qanswers = new ArrayList<Double>();
					for (String qtask : qtasks) {
						try {
							double qans = dataset.getGoldAnswer(worker, qtask);
							validqtasks.add(qtask);
							qanswers.add(qans);
						} catch (Exception e) {}
					}
					crowdFramework.submitQTaskAnswers(worker, validqtasks, qanswers);
				}

				String task = crowdFramework.assignTask(worker); // assign task

				if (qtasks != null) {
					if (task == null) {
						underqualified.add(worker);
						// If a worker who submitted qualification test 
						//    still cannot be assigned with any task, 
						//    we regard this worker under-qualified and 
						//    omit her request in the following assignments. 
					}
				}

				if (task != null) {
					double answer = dataset.getGoldAnswer(worker, task);
					crowdFramework.submitAnswer(task, worker, answer);
				}
				long t2 = System.currentTimeMillis();
				latency += (t2 - t1);
				asgnum++;
			}
			worker = cwp.getRequestingWorker();
		}

		// Output the assignments and aggregated results
		System.out.println("Average latency of each assignment: " + latency
				+ " ms / " + asgnum + " = " + latency / asgnum + " ms");

		Map<String, Integer> task2result = crowdFramework.aggregateAnswers();

		outputAssignments(crowdFramework.getWorkerTaskAssignment(), outfolder
				+ "assignments.txt");
		outputFinalResult(task2result, outfolder + "final_result.txt");

		// Print the value of the evaluation metric, i.e., accuracy in current
		// version
		EvalMetrics metrics = new EvalMetrics();
		metrics.evaluteAccuracy(dataset.getTasks(), task2result);
	}

	private static TaskAssigner buildTaskAssigner(String assignerName,
			DatasetLoader dataset) {
		TaskAssigner assigner = null;
		if (assignerName.equals("RandomAssign")) {
			assigner = new RandomAssigner();
		} else if (assignerName.equals("AvgAccAssign")) {
			assigner = new AvgAccAssigner();
		} else if (assignerName.equals("AdaptiveAssign")) {
			assigner = new AdaptiveAssigner(dataset.getPPVFilename());
		}
		return assigner;
	}

	private static AnswerAggregator buildAnswerAggregator(String aggregatorName) {
		AnswerAggregator aggregator = null;
		if (aggregatorName.equals("MajorityVote")) {
			aggregator = new MVAggregator();
		} else if (aggregatorName.equals("EMVote")) {
			aggregator = new EMAggregator();
		} else if (aggregatorName.equals("ProbVote")) {
			aggregator = new PVAggregator();
		}
		return aggregator;
	}

	private static void outputAssignments(AsgMatrix asg, String outfile)
			throws Exception {
		BufferedWriter w = new BufferedWriter(new FileWriter(outfile));
		for (String worker : asg.worker2tasks.keySet()) {
			Map<String, Double> taskAnswers = asg.worker2tasks.get(worker);
			for (String task : taskAnswers.keySet()) {
				double answer = taskAnswers.get(task);
				w.write(worker + "\t" + task + "\t" + answer);
				w.newLine();
			}
		}
		w.close();
	}

	private static void outputFinalResult(Map<String, Integer> task2result,
			String outfile) throws Exception {
		BufferedWriter w = new BufferedWriter(new FileWriter(outfile));
		for (String task : task2result.keySet()) {
			int result = task2result.get(task);
			w.write(task + "\t" + result);
			w.newLine();
		}
		w.close();
	}
}
