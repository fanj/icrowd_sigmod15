package icrowd.core.aggregator;

import icrowd.core.AsgMatrix;
import icrowd.core.estimate.Estimator;

import java.util.*;

public class MVAggregator implements AnswerAggregator {

	public Map<String, Integer> aggregateAnswers(List<String> allTasks, 
			AsgMatrix asg, Estimator est) {
		Map<String, Integer> task2result = new HashMap<String, Integer> ();
		Map<String, List<String>> task2pws = new HashMap<String, List<String>> ();
		Map<String, List<String>> task2nws = new HashMap<String, List<String>> ();
		for (String worker : asg.worker2tasks.keySet()) {
			
			Map<String, Double> tasks = asg.worker2tasks.get(worker);
			for (String task : tasks.keySet()) {
				Double score = tasks.get(task);
				Map<String, List<String>> map = null;
				if (score == 1.0) map = task2pws;
				else if (score == 0.0) map = task2nws;
				List<String> ws = map.get(task);
				if (ws == null) ws = new ArrayList<String> ();
				ws.add(worker);
				map.put(task, ws);
			}
		}
		
		for (String task : allTasks) {
			int decision = 1;
			List<String> pws = task2pws.get(task);
			List<String> nws = task2nws.get(task);
			
			if (pws != null && nws == null) decision = 1;
			else if (pws == null && nws != null) decision = 0;
			else if (pws == null && nws == null) continue;
			else if (pws.size() > nws.size()) decision = 1;
			else if (pws.size() < nws.size()) decision = 0; 
			else {
				if (Math.random() <= 0.5) decision = 1;
				else decision = 0;
			}
			task2result.put(task, decision);
		}
		return task2result;
	}
	
}
