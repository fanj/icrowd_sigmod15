package icrowd.core.aggregator;

import icrowd.core.AsgMatrix;
import icrowd.core.estimate.Estimator;
import icrowd.core.aggregator.em.*;

import java.util.*;
import java.io.*;

public class EMAggregator implements AnswerAggregator {

	public final static String tempFolder = "temp/";

	public Map<String, Integer> aggregateAnswers(List<String> allTasks,
			AsgMatrix asg, Estimator est) {
		Map<String, Integer> task2result = new HashMap<String, Integer>();
		try {
			Map<String, List<Submission>> task2subs = new HashMap<String, List<Submission>>();
			Map<String, String> grounds = new HashMap<String, String>();
			for (String worker : asg.worker2tasks.keySet()) {

				Map<String, Double> tasks = asg.worker2tasks.get(worker);
				for (String task : tasks.keySet()) {
					int answer = tasks.get(task).intValue();
					Submission sub = new Submission(worker, task,
							String.valueOf(answer), null, null, null);
					List<Submission> subs = task2subs.get(task);
					if (subs == null)
						subs = new ArrayList<Submission>();
					subs.add(sub);
					task2subs.put(task, subs);
				}
			}

			for (String task : allTasks) {
				grounds.put(task, "1");
				// put a dummy ground-truth as it is not useful in aggregation
			}

			GetAnotherLabel gal = new GetAnotherLabel(task2subs, grounds);
			int iter = 100;
			gal.EstimateMaximize(iter, tempFolder);

			BufferedReader r = new BufferedReader(new FileReader(tempFolder
					+ "post-majority-vote.txt"));
			String line = r.readLine();
			while (line != null) {
				String[] tmps = line.split("\t");
				String task = "";
				for (int i = 0; i < tmps[0].split("_").length - 1; i++) {
					task += tmps[0].split("_")[i] + "_";
				}
				task = task.substring(0, task.length() - 1);
				int result = Integer.parseInt(tmps[1]);
				task2result.put(task, result);
				line = r.readLine();
			}
			r.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return task2result;
	}

}
