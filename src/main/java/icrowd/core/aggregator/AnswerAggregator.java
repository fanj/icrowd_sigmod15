package icrowd.core.aggregator;

import icrowd.core.AsgMatrix;
import icrowd.core.estimate.Estimator;

import java.util.*;

public interface AnswerAggregator {

	public abstract Map<String, Integer> aggregateAnswers(
			List<String> allTasks, AsgMatrix asg, Estimator est);
}
