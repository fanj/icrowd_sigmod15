package icrowd.core.aggregator.em;

import java.util.*;
import java.io.*;

/**
 * This is a java implementation of the following paper: 
 * 
 *   Victor S. Sheng, Foster J. Provost, Panagiotis G. Ipeirotis: 
 *   Get another label? improving data quality and data mining using multiple, noisy labelers. 
 *   KDD 2008:614-622
 *   
 * @author Ju Fan
 *
 */

public class GetAnotherLabel {
	
	class Tuple {
		String str1;
		String str2;
		
		public Tuple (String s1, String s2) {
			str1 = s1;
			str2 = s2;
		}
	}
	
	public static final String l0 = "0";
	public static final String l1 = "1";
	
	List<String[]> worker_example_label_set = new LinkedList<String[]> ();
	Map<String, List<Tuple>> example_to_worker_label = 
			new HashMap<String, List<Tuple>> ();
	
	Map<String, List<Tuple>> worker_to_example_label = 
			new HashMap<String, List<Tuple>> ();
	
	Set<String> label_set = new HashSet<String> ();
	Map<String, Integer> label_count = 
			new HashMap<String, Integer> ();
	
	public GetAnotherLabel (Map<String, List<Submission>> taskStat, 
			Map<String, String> tasks) throws Exception {
		for (String task : taskStat.keySet()) {
			String trueAnswer = tasks.get(task);
			String example = task + "_" + trueAnswer;
			
			for (Submission sub : taskStat.get(task)) {
				String worker = sub.mWorkerId;
				String answer = sub.mTaskAnswer;
				String label = answer;
				String items[] = {worker, example, label};
				this.worker_example_label_set.add(items);
				List<Tuple> tuples = this.example_to_worker_label.get(items[1]);
				if (tuples == null) {
					tuples = new LinkedList<Tuple> ();
					this.example_to_worker_label.put(items[1], tuples);
				}
				tuples.add(new Tuple(items[0], items[2]));
				
				tuples = this.worker_to_example_label.get(items[0]);
				if (tuples == null) {
					tuples = new LinkedList<Tuple> ();
					this.worker_to_example_label.put(items[0], tuples);
				}
				tuples.add(new Tuple(items[1], items[2]));
				
				Integer count = this.label_count.get(items[2]);
				if (count == null) {
					count = 0;
				}
				count ++;
				this.label_count.put(items[2], count);
			}
		}
		this.label_set = this.label_count.keySet();
	}
	
	public GetAnotherLabel (String filename) throws Exception {
		BufferedReader r = new BufferedReader (new FileReader(filename));
		String line = r.readLine();
		while (line != null) {
			String items[] = line.split("\t");
			this.worker_example_label_set.add(items);
			
			List<Tuple> tuples = this.example_to_worker_label.get(items[1]);
			if (tuples == null) {
				tuples = new LinkedList<Tuple> ();
				this.example_to_worker_label.put(items[1], tuples);
			}
			tuples.add(new Tuple(items[0], items[2]));
			
			tuples = this.worker_to_example_label.get(items[0]);
			if (tuples == null) {
				tuples = new LinkedList<Tuple> ();
				this.worker_to_example_label.put(items[0], tuples);
			}
			tuples.add(new Tuple(items[1], items[2]));
			
			Integer count = this.label_count.get(items[2]);
			if (count == null) {
				count = 0;
			}
			count ++;
			this.label_count.put(items[2], count);
			line = r.readLine();
		}
		this.label_set = this.label_count.keySet();
		r.close();
	}

	public Map<String, String> MajorityVote () {
		Map<String, String> example_to_decision_label = 
				new HashMap<String, String> ();
		Map<String, Map<String, Double>> example_to_label_weight = 
				new HashMap<String, Map<String, Double>> ();
		for (String items[] : this.worker_example_label_set) {
			String example = items[1];
			String label = items[2];
			
			Map<String, Double> label_weight = example_to_label_weight.get(example);
			if (label_weight == null) {
				label_weight = new HashMap<String, Double>();
			}
			Double weight = label_weight.get(label);
			if (weight == null) {
				weight = 0.0;
			}
			weight ++;
			label_weight.put(label, weight);
			example_to_label_weight.put(example, label_weight);			
		}
		for (String example : example_to_label_weight.keySet()) {
			Map<String, Double> label_weight = example_to_label_weight.get(example);
			String max_label = null;
			double max_weight = 0;
			for (String label: label_weight.keySet()) {
				double weight = label_weight.get(label);
				if (weight > max_weight) {
					max_weight = weight;
					max_label = label;
				}
			}
			example_to_decision_label.put(example, max_label);
		}
		return example_to_decision_label;
 		
	}
	
	public Map<String, Map<String, Double>> InitSoftLabel () {
		Map<String, Map<String, Double>> example_to_softlabel = 
				new HashMap<String, Map<String, Double>> ();
		for (String items[] : this.worker_example_label_set) {
			String example = items[1];
			String label = items[2];
			
			Map<String, Double> softlabel = example_to_softlabel.get(example);
			if (softlabel == null) {
				softlabel = new HashMap<String, Double>();
			}
			Double count = softlabel.get(label);
			if (count == null) {
				count = 0.0;
			}
			count ++;
			softlabel.put(label, count);
			example_to_softlabel.put(example, softlabel);
		}
		for (String example: example_to_softlabel.keySet()) {
			Map<String, Double> softlabel = example_to_softlabel.get(example);
			double label_num = 0.0;
			for (String label : softlabel.keySet()) {
				label_num += softlabel.get(label);
			}
			for (String label : softlabel.keySet()) {
				double sf = softlabel.get(label);
				sf = sf / label_num;
				softlabel.put(label, sf);
			}
		}
		return example_to_softlabel;
	}
	
	public Map<String, Map<String, Map<String, Double>>> ConfusionMatrix (
			Map<String, Map<String, Double>> example_to_softlabel) {
		Map<String, Map<String, Map<String, Double>>>  worker_to_confusion_matrix = 
				new HashMap<String, Map<String, Map<String, Double>>>  ();
		Map<String, Map<String, Double>> worker_to_finallabel_weight = 
				new HashMap<String, Map<String, Double>> ();
		
		Map<String, Map<String, Map<String, Double>>> worker_to_finallabel_workerlabel_weight = 
				new HashMap<String, Map<String, Map<String, Double>>> ();
		
		for (String worker : worker_to_example_label.keySet()) {
			List<Tuple> example_label = worker_to_example_label.get(worker);
			if (!worker_to_finallabel_weight.containsKey(worker)) {
				worker_to_finallabel_weight.put(worker, 
						new HashMap<String, Double> ());
			}
			if (!worker_to_finallabel_workerlabel_weight.containsKey(worker)) {
				worker_to_finallabel_workerlabel_weight.put(worker, 
						new HashMap<String, Map<String, Double>> ());
			}
			for (Tuple tuple : example_label) {
				String example = tuple.str1;
				String workerlabel = tuple.str2;
				Map<String, Double> softlabel = 
						example_to_softlabel.get(example);
				for (String finallabel : softlabel.keySet()) {
					double weight = softlabel.get(finallabel);
					Map<String, Double> finallabel_weight = worker_to_finallabel_weight.get(worker);
					Double fw = finallabel_weight.get(finallabel);
					if (fw == null) {
						fw = 0.0;
					}
					fw += weight;
					finallabel_weight.put(finallabel, fw);
					
					if (!worker_to_finallabel_workerlabel_weight.get(worker).containsKey(finallabel)) {
						worker_to_finallabel_workerlabel_weight.get(worker).put(finallabel, 
								new HashMap<String, Double>());
					}
					Double fww = worker_to_finallabel_workerlabel_weight.get(worker).get(finallabel).get(workerlabel);
					if (fww == null) {
						fww = 0.0;
					}
					fww += weight;
					worker_to_finallabel_workerlabel_weight.get(worker).get(finallabel).put(workerlabel, fww);
				}
			}
		}
		
		worker_to_confusion_matrix = worker_to_finallabel_workerlabel_weight;
		
		for (String worker : worker_to_finallabel_workerlabel_weight.keySet()) {
			Map<String, Map<String, Double>> finallabel_workerlabel_weight = 
					worker_to_finallabel_workerlabel_weight.get(worker);
			for (String finallabel : finallabel_workerlabel_weight.keySet() ) {
				Map<String, Double> workerlabel_weight = finallabel_workerlabel_weight.get(finallabel);
				for (String workerlabel : workerlabel_weight.keySet()) {
					double weight = workerlabel_weight.get(workerlabel);
					if (worker_to_finallabel_weight.get(worker).get(finallabel) == 0.0) {
						assert weight == 0;
					} else {
						double w = worker_to_confusion_matrix.get(worker).get(finallabel).get(workerlabel);
						w = weight / worker_to_finallabel_weight.get(worker).get(finallabel);
						worker_to_confusion_matrix.get(worker).get(finallabel).put(workerlabel, w);
					}
				}
			}
		}
		
		String[] label_set = {l0,l1};
		for (String worker : worker_to_confusion_matrix.keySet()) {
			Map<String, Map<String, Double>> confusion_matrix = 
					worker_to_confusion_matrix.get(worker);
			for (String label : label_set) {
				if (!confusion_matrix.containsKey(label)) {
					Map<String, Double> tmp = new HashMap<String, Double> ();
					tmp.put(label, 1.0);
					confusion_matrix.put(label, tmp);
				}
				//System.out.println(confusion_matrix);
				if (!confusion_matrix.get(label).containsKey(l0)) {
					confusion_matrix.get(label).put(l0, 
							1 - confusion_matrix.get(label).get(l1));
				} else if (! confusion_matrix.get(label).containsKey(l1)) {
					confusion_matrix.get(label).put(l1, 
							1 - confusion_matrix.get(label).get(l0));
				}
			}
		}
		return worker_to_confusion_matrix;
		
	}
            
	public Map<String, Double> PriorityProbability (
			Map<String, Map<String, Double>> example_to_softlabel) {
		Map<String, Double> label_to_priority_probability = 
				new HashMap<String, Double> ();
		for (String example : example_to_softlabel.keySet()) {
			Map<String, Double> softlabel = example_to_softlabel.get(example);
			for (String label : softlabel.keySet()) {
				double probability = softlabel.get(label);
				Double p = label_to_priority_probability.get(label);
				if (p == null) {
					p = 0.0;
				}
				p += probability;
				label_to_priority_probability.put(label, p);
			}
		}
		for (String label : label_to_priority_probability.keySet()) {
			double count = label_to_priority_probability.get(label);
			count /= (double)example_to_softlabel.size();
			label_to_priority_probability.put(label, count);
		}
		return label_to_priority_probability;
	}
	
	public Map<String, Map<String, Double>> ProbabilityMajorityVote (
			Map<String, Double> label_to_priority_probability, 
			Map<String, Map<String, Map<String, Double>>> worker_to_confusion_matrix) {
		Map<String, Map<String, Double>> example_to_sortlabel = 
				new HashMap<String, Map<String, Double>> ();
		for (String example: example_to_worker_label.keySet()) {
			List<Tuple> worker_label_set = example_to_worker_label.get(example);
			Map<String, Double> sortlabel = new HashMap<String, Double> ();
			double total_weight = 0.0;
			for (String final_label : label_to_priority_probability.keySet()) {
				double priority_probability = label_to_priority_probability.get(final_label);
				double weight = priority_probability;
				for (Tuple tuple : worker_label_set) {
					String worker = tuple.str1;
					String worker_label = tuple.str2;
					try {
						weight *= worker_to_confusion_matrix.get(worker).get(final_label).get(worker_label);
					} catch (Exception e) {
						weight = 0;
					}
				}
				total_weight += weight;
				sortlabel.put(final_label, weight);
			}
			for (String final_label : sortlabel.keySet()) {
				double weight = sortlabel.get(final_label);
				if (total_weight == 0) {
					assert weight == 0;
				} else {
					sortlabel.put(final_label, weight/total_weight);
				}
			}
			example_to_sortlabel.put(example, sortlabel);
		}
		return example_to_sortlabel;
	}
    
    public void OutputDecisionLabel (String filename, 
    		Map<String, Map<String, Double>> example_to_softlabel) 
    	throws Exception {
    	BufferedWriter writer = new BufferedWriter (new FileWriter(filename));
    	TreeMap<String, Map<String, Double>> sorted_example_to_softlabel = 
    			new TreeMap<String, Map<String, Double>> ();
    	for (String example : example_to_softlabel.keySet()) {
    		sorted_example_to_softlabel.put(example, 
    				example_to_softlabel.get(example));
    	}
    	for (String example : sorted_example_to_softlabel.keySet()) {
    		Map<String, Double> softlabel = sorted_example_to_softlabel.get(example);
    		String decision_label = null;
    		double max = 0.0;
    		for (String label : softlabel.keySet()) {
    			double w = softlabel.get(label);
    			if (w > max) {
    				max = w;
    				decision_label = label;
    			}
    		}
    		writer.write(example + "\t" + decision_label);
    		writer.newLine();
    	}
    	writer.close();
    }
      

    private class SortItem implements Comparable <Object>{
    	double key;
    	List content;
    	
		public int compareTo(Object arg0) {
			SortItem item = (SortItem) arg0;
			if (key > item.key) {
				return 1;
			} else if (key < item.key){
				return -1;
			} else {
				String e1 = (String)content.get(0);
				String e2 = (String)item.content.get(0);
				
				String items1[] = e1.split("_");
				String items2[] = e2.split("_");
				
				String key1 = items1[1];
				String key2 =  items2[1];
				
				if (key1.compareTo(key2) > 0) {
					return 1;
				} else {
					return -1;
				}
			}
		} 
	
		
    }
        

    public void OutputSoftLabel (String filename, Map<String, 
    		Map<String, Double>> example_to_softlabel) throws Exception {
    	BufferedWriter w = new BufferedWriter (new FileWriter(filename));
    	//DecimalFormat df = new DecimalFormat ("0.000000000000");
    	TreeSet<SortItem> tmp = new TreeSet<SortItem> ();
    
    	for (String example : example_to_softlabel.keySet()) {
    		double key = example_to_softlabel.get(example).get(l1);
    		//key = Double.parseDouble(df.format(key));
    		example_to_softlabel.get(example).put(l1, key);
    		List tmpL = new ArrayList();
    		tmpL.add(example);
    		tmpL.add(example_to_softlabel.get(example));
    		SortItem item = new SortItem();
    		item.key = -key;
    		item.content = tmpL;
    		tmp.add(item);
    	}
    	
    	for (SortItem item : tmp) {
    		List tmpL = (List)item.content;
    		String example =(String) tmpL.get(0);
    		Map<String, Double> softlabel = (Map<String, Double>) tmpL.get(1);
    		w.write(example + "\t" + softlabel.get(l1));
    		w.newLine();
    	}
    	w.close();
    }
    

    public void SetArray (Map<String, Map<String, Double>> array, 
    		String label_0, String label_1, double value) {
    	Map<String, Double> tmp = array.get(label_0);
    	if (tmp == null) {
    		tmp = new HashMap<String, Double> ();
    		array.put(label_0, tmp);
    	}
    	tmp.put(label_1, value);
    }
    
    public double GetArray (Map<String, Map<String, Double>> array, 
    		String label_0, String label_1) {
    	if (! array.containsKey(label_0)) {
    		return 0;
    	}
    	Double ret = array.get(label_0).get(label_1);
    	if (ret == null) {
    		ret = 0.0;
    	}
    	return ret;
    }
    
    double MyDivide(double a, double b) {
    	if (b == 0) {
    		return 0;
    	}
    	return a / b;
    }
    
    Map<String, Double[]> WorkerQuality (
    		Map<String, Map<String, Map<String, Double>>> worker_to_confusion_matrix, 
    		Map<String, Double> label_to_priority_probability,
    		Map<String, Map<String, Double>> cost_matrix) {
    	
    	Map<String, Double[]> worker_to_quality = new HashMap<String, Double[]> ();
    	Set<String> label_set = label_to_priority_probability.keySet();
    	//System.out.println (label_to_priority_probability);
    	for (String worker :worker_to_confusion_matrix.keySet()) {
    		Map<String, Map<String, Double>> confusion_matrix = 
    				worker_to_confusion_matrix.get(worker);
    		Map<String, Map<String, Double>> reverse_confusion_matrix = 
    				new HashMap<String, Map<String, Double>> ();
    		for (String label_0 : label_set) {
    			for (String label_1 : label_set) {
    				double value = this.GetArray(confusion_matrix,label_0,  label_1) * 
    						label_to_priority_probability.get(label_0);
    				this.SetArray(reverse_confusion_matrix, label_0, label_1, value);
    			}
    		}
    		Map<String, Double> label_pr = new HashMap<String, Double> ();
    		for (String label_0 : label_set) {
    			double pr = 0.0;
    			for (String label_1 : label_set) {
    				pr += reverse_confusion_matrix.get(label_0).get(label_1);
    			}
    			label_pr.put(label_0, pr);
    		}
    		
    		for (String label_0 : label_set) {
    			for (String label_1 : label_set) {
    				double value = this.MyDivide(reverse_confusion_matrix.get(label_0).get(label_1), 
    						label_pr.get(label_0));
    				reverse_confusion_matrix.get(label_0).put(label_1, value);
    			}
    		}
    		double total_cost = 0.0;
//    		System.out.println (worker);
//    		System.out.println (confusion_matrix);
//    		System.out.println (reverse_confusion_matrix);
    		
    		double spammer_cost = 0.0;
    		for (String label_0 : label_set) {
    			double cost = 0.0;
    			for (String label_1 : label_set) {
    				for (String label_2 : label_set) {
    					cost += reverse_confusion_matrix.get(label_0).get(label_1) * 
    							reverse_confusion_matrix.get(label_0).get(label_2) * 
    							cost_matrix.get(label_1).get(label_2);
    					spammer_cost += label_to_priority_probability.get(label_1) * 
    							label_to_priority_probability.get(label_2) * 
    							cost_matrix.get(label_1).get(label_2);
    					
    				}
    			}
    			total_cost += cost * label_pr.get(label_0);
    		}
    		double precision = reverse_confusion_matrix.get(l1).get(l1);
    		double recall = confusion_matrix.get(l1).get(l1);
    		double fmeasure = MyDivide(2*precision*recall, precision+recall);
    		Double[] array = {precision, recall, fmeasure, 1-total_cost/spammer_cost};
    		worker_to_quality.put(worker, array);
    	}
    	return worker_to_quality;
    }
    
    
    public void EstimateMaximize (int iter, String folder) throws Exception {
    	// Inital Step
    	Map<String, Map<String, Map<String, Double>>> worker_to_confusion_matrix = 
    			new HashMap<String, Map<String, Map<String, Double>>> ();
    	Map<String, Double> label_to_priority_probability = 
    			new HashMap<String, Double> ();
    	Map<String, Map<String, Double>> example_to_softlabel =
    			InitSoftLabel();
    	this.OutputDecisionLabel(folder + "pre-majority-vote.txt", example_to_softlabel);
    	while (iter > 0) {
    		label_to_priority_probability = 
    				this.PriorityProbability(example_to_softlabel);
    		//System.out.println (label_to_priority_probability);
    		worker_to_confusion_matrix = 
    				this.ConfusionMatrix(example_to_softlabel);
    		for (String worker : worker_to_confusion_matrix.keySet()) {
    			Map<String, Map<String, Double>> confusion_matrix = 
    					worker_to_confusion_matrix.get(worker);
    			//System.out.println(worker + " " + confusion_matrix);
    		}
    		example_to_softlabel = this.ProbabilityMajorityVote(label_to_priority_probability, worker_to_confusion_matrix);
    		iter --;
    	}
    	OutputDecisionLabel(folder + "post-majority-vote.txt", example_to_softlabel);
    	OutputSoftLabel(folder + "softlabel.txt", example_to_softlabel);
    	Map<String, Map<String, Double>>  cost_matrix = 
    			new HashMap<String, Map<String, Double>> ();
    	for (String label_0 : this.label_set) {
    		for (String label_1 : this.label_set) {
    			double setValue = 0;
    			if (!label_0.equals(label_1)) {
    				setValue = 1;
    			}
    			SetArray(cost_matrix, label_0, label_1, setValue);
    		}
    	}
    	Map<String, Double[]> worker_to_quality = 
    			WorkerQuality (worker_to_confusion_matrix, label_to_priority_probability, cost_matrix);
    	OutputWorkerQuality (folder + "worker-quality.txt", worker_to_quality);
    }
    
    Map<String, Double> InitPriorityProbability () {
    	Map<String, Double> label_to_priority_probability = 
    			new HashMap<String, Double> ();
    	for (String label : this.label_set) {
    		double value = 1.0 / (double) this.label_set.size();
    		label_to_priority_probability.put(label, value);
    	}
    	return label_to_priority_probability;
    }
    
    Map<String, Map<String, Map<String, Double>>> InitConfusionMatrix (Set<String> workers) {
    	Map<String, Map<String, Map<String, Double>>> worker_to_confusion_matrix = 
    			new HashMap<String, Map<String, Map<String, Double>>> ();
    	
    	for (String worker : workers) {
    		if (! worker_to_confusion_matrix.containsKey(worker)) {
    			worker_to_confusion_matrix.put(worker, new HashMap<String, Map<String, Double>> ());
    		}
    		for (String label1 : label_set) {
    			if (!worker_to_confusion_matrix.get(worker).containsKey(label1)) {
    				worker_to_confusion_matrix.get(worker).put(label1, new HashMap<String, Double> ());
    			}
    			for (String label2 : label_set) {
    				if (label1.equals(label2)) {
    					worker_to_confusion_matrix.get(worker).get(label1).put(label2, 0.9);
    				} else {
    					double value = 0.1 / (double) (label_set.size() - 1);
    					worker_to_confusion_matrix.get(worker).get(label1).put(label2, value);
    				}
    			}
    		}
    	}
    	return worker_to_confusion_matrix;
    }

    
    public void OutputWorkerQuality (String filename, 
    		Map<String, Double[]> worker_to_quality) throws Exception {
    	BufferedWriter w = new BufferedWriter (new FileWriter (filename));
    	for (String worker : worker_to_quality.keySet()) {
    		Double[] quality = worker_to_quality.get(worker);
    		w.write(worker + "\t");
    		w.write(quality[0] + "\t" + quality[1] + "\t" +
    				quality[2] + "\t" + quality[3]);
    		w.newLine();
    	}
    	w.close();
    }
   
  
}