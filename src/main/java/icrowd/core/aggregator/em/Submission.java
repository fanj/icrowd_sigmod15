package icrowd.core.aggregator.em;

public class Submission {
	public String mWorkerId;
	public String mTaskId;
	public String mTaskAnswer;
	
	public Long mSubmitTime;
	
	public String mHitId;
	public String mAsgId;
	
	public Submission (String workerId, String taskId, String taskAnswer, 
			Long submitTime, String hitId, String asgId) {
		mWorkerId = workerId;
		mTaskId = taskId;
		mTaskAnswer = taskAnswer;
		mSubmitTime = submitTime;
		mHitId = hitId;
		mAsgId = asgId;
	}	
	
	public String toString () {
		return mTaskId + "," + mWorkerId + "," + mTaskAnswer;
	}
}
