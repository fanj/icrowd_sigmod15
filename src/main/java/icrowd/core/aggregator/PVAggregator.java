package icrowd.core.aggregator;

import icrowd.core.AsgMatrix;
import icrowd.core.estimate.Estimator;
import icrowd.utils.ScoredItem;

import java.util.*;

public class PVAggregator implements AnswerAggregator {

	/**
	 * The aggregation strategy is from CDAS (see the VLDB 2012 paper)
	 */
	public Map<String, Integer> aggregateAnswers(List<String> allTasks,
			AsgMatrix asg, Estimator est) {
		Map<String, Integer> task2result = new HashMap<String, Integer> ();
		Map<String, List<String>> task2pws = new HashMap<String, List<String>> ();
		Map<String, List<String>> task2nws = new HashMap<String, List<String>> ();
		for (String worker : asg.worker2tasks.keySet()) {
			Map<String, Double> tasks = asg.worker2tasks.get(worker);
			for (String task : tasks.keySet()) {
				Double score = tasks.get(task);
				Map<String, List<String>> map = null;
				if (score == 1.0) map = task2pws;
				else if (score == 0.0) map = task2nws;
				List<String> ws = map.get(task);
				if (ws == null) ws = new ArrayList<String> ();
				ws.add(worker);
				map.put(task, ws);
			}
		}
		for (String task : allTasks) {
			List<String> pws = task2pws.get(task);
			List<String> nws = task2nws.get(task);
			int decision = -1;
			
			double part1 = 1.0;
			double part0 = 1.0;
			
			if (pws != null) {
				for (String pw : pws) {
					double score = est.getAccuracy (task, pw);
					part1 *= (score);
					part0 *= (1 - score);
				}
			}
			
			if (nws != null) {
				for (String nw : nws) {
					double score = est.getAccuracy (task, nw);
					part1 *= (1 - score);
					part0 *= score;
				}
			}
			
			if (part1 > part0) {
				decision = 1;
			} else { 
				decision = 0;
			}
			
			if (pws != null && nws == null) { // empty negative answers
				if (pws.size() > 1) decision = 1; // if more agree on `1`
			} else if (nws != null && pws == null) { // empty positive answers
				if (nws.size() > 1) decision = 0; // if more agree on `0`
			}
			task2result.put(task, decision);
			
			// For debugging usage only
			printVotings (task, pws, nws, decision, est);
			
		}
		return task2result;
	}
	
	public void printVotings (String task, List<String> pws, List<String> nws, 
			int decision, Estimator est) {
		boolean printdetail = true;
		if (printdetail) {
		List<ScoredItem> ans = new ArrayList<ScoredItem>();
			if (pws != null) {
				for (String pw : pws) {
					double score = est.getAccuracy(task, pw);
					ScoredItem item = new ScoredItem (pw + "_1" , 
							score);
					ans.add(item);
				}
			}
			if (nws != null) {	
				for (String nw : nws) {
					double score = est.getAccuracy(task, nw);
					ScoredItem item = new ScoredItem (nw + "_0" , 
							score);
					ans.add(item);
				}
			}
			Collections.sort(ans);
			System.out.println(decision + "\t" + task + "\t" + ans);
		}
	}

}
