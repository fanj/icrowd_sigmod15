package icrowd.core.assigner;

import icrowd.core.estimate.Estimator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * The most straightforward task assigner, 
 *   which assigns tasks randomly while following rules in access control
 * @author Ju Fan
 *
 */
public class RandomAssigner extends TaskAssigner {

	/**
	 * Implement a random task assignment strategy
	 */
	public String assignTask(String worker) {
		List<String> candidates = new ArrayList<String> ();
		for (String task : this.mTasks) {
			if (accCtr.isValid(worker, task)) { 
				candidates.add(task);
			}
		}
		if (candidates.isEmpty()) return null; // no task to be assigned
		Random random = new Random();
		int randomPos = random.nextInt(candidates.size());
		String task = candidates.get(randomPos);
		return task;
	}
	
	/**
	 * Random Assigner does not need to consider if a task is globally completed
	 */
	public void globallyComplete(String task, List<String> asgWs,
			List<Double> asgAs) {	}

	/**
	 * Random Assigner does not need to consider qualification
	 */
	public void submitQTaskAnswer(String worker, List<String> qtasks,
			List<Double> qanswers) { }

	/**
	 * The random assigner does not use any estimator, 
	 *   so this function just returns null. 
	 */
	public Estimator getEstimator() {
		return null;
	}

	/**
	 * The random assigner does not need to update itself
	 * 		when a task has been completed by a worker
	 */
	public void complete(String task, String worker) {	}

}
