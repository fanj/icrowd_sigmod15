package icrowd.core.assigner;

import icrowd.core.estimate.Estimator;
import icrowd.core.estimate.GraphEstimator;
import icrowd.utils.*;

import java.util.*;

public class AdaptiveAssigner extends TaskAssigner {
	GraphEstimator est = null;
	//Set<String> activeWorkers = new HashSet<String> ();
	// the set of current active workers
	
	/**
	 * task --> top workers with accuracies
	 * We do not maintain full ranking, but keep top-k.
	 */
	public Map<String, SortedList> task2topws = 
			new HashMap<String, SortedList> (); 
	
	Map<String, List<String>> worker2rtopts = 
			new HashMap<String, List<String>> (); // worker --> reverse top workers

	
	public AdaptiveAssigner (String ppvfilename) {
		try {
		est = new GraphEstimator (ppvfilename);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void submitQTaskAnswer(String worker, List<String> qtasks,
			List<Double> qanswers) {
		// As the worker is new, we first initialize the accuracies
		for (String task : this.mTasks) {
			if (accCtr.isValid(worker, task)) {
				est.initAccuracy(task, worker);
			}
		}
		/**
		 * Update accuracy estimation based on QTasks
		 */
		Map<String, Map<String, Double>> task2wperfs = 
				new HashMap<String, Map<String, Double>> ();
		for (int i = 0; i < qtasks.size(); i ++) {
			String qtask = qtasks.get(i);
			double qans = qanswers.get(i);
			Map<String, Double> wperfs = new HashMap<String, Double> ();
			wperfs.put(worker, qans); // the performance
			task2wperfs.put(qtask, wperfs);
		}
		Map<String, Set<String>> worker2updatedTasks = est.updateOnPerfs (task2wperfs);
		
		/**
		 * Update top worker sets & reverse task sets
		 */
		Set<String> updatedTasks = worker2updatedTasks.get(worker);
		if (updatedTasks == null) {
			updatedTasks = new HashSet<String> ();
			for (String t : mTasks) updatedTasks.add(t);
		}
		for (String task : updatedTasks) {
			//if (accCtr.isGloballyCompleted(task)) continue;
			if (!accCtr.isValid(worker, task)) continue; // no need to update invalid assignment
			SortedList topws = task2topws.get(task);
			if (topws == null ) {
				topws = new SortedList (asgSize);
				task2topws.put(task, topws);
			}
			Map<String, Boolean> upgrade = topws.update(worker, est.getAccuracy(task, worker));
			updateWorker2Rtopts(task, upgrade);
		}
	}

	
	public String assignTask(String worker) {
		//activeWorkers.add(worker);
		List<String> rtopts = this.getRTopkTasks(worker);
		// For Debug Usage
//		if (worker.endsWith("ZP4") && rtopts.isEmpty()) {
//			List<String> candidates = new ArrayList<String> ();
//			for (String task : this.mTasks) {
//				if (accCtr.isValid(worker, task)) { 
//					candidates.add(task);
//				}
//			}
//			if (!candidates.isEmpty())
//				System.out.println("\t Empty RTasks: " + worker + "\t" + candidates);
//		}
		Map<String, String> globalAsg = updateGlobalAssign(worker, rtopts);
		String task = globalAsg.get(worker);
		return task;
	}
	
	public List<String> getRTopkTasks (String worker) {
		List<String> rtopts = worker2rtopts.get(worker);
		if (rtopts == null) return new ArrayList<String> ();
		return rtopts;
	}
	
	public List<ScoredItem> getRTopk (String worker) {
		List<ScoredItem> items = new ArrayList<ScoredItem> ();
		List<String> rtopts = worker2rtopts.get(worker);
		if (rtopts != null) {
			for (String task : rtopts) {
				double acc = est.getAccuracy(task, worker);
				ScoredItem item = new ScoredItem (task, acc);
				items.add(item);
			}
		}
		return items;
	}

	
	public void globallyComplete(String glbtask, List<String> asgWs, List<Double> asgAs) {
		Map<String, Map<String, Double>> task2wperfs = 
				est.computeWorkerPerfs (glbtask, asgWs, asgAs);
		
		Map<String, Set<String>> worker2updatedTasks = 
				est.updateOnPerfs(task2wperfs);
		
		if (task2topws.containsKey(glbtask))
			task2topws.get(glbtask).clear(); // clear the top worker set
		 
		// Change the status
		for (String w : worker2rtopts.keySet()) {
			List<String> wrtops = worker2rtopts.get(w);
			wrtops.remove(glbtask); // remove the task
		}
		
		/**
		 * Update top worker sets & reverse task sets
		 */
		for (String w : worker2updatedTasks.keySet()) {
			Set<String> updatedTasks = worker2updatedTasks.get(w);
			for (String task : updatedTasks) {
				//if (accCtr.isGloballyCompleted(task)) continue;
				if (!accCtr.isValid(w, task)) continue; // no need to update invalid assignment
				SortedList topws = task2topws.get(task);
				if (topws == null ) {
					topws = new SortedList (asgSize);
					task2topws.put(task, topws);
				}
				Map<String, Boolean> upgrade = topws.update(w, est.getAccuracy(task, w));
				updateWorker2Rtopts(task, upgrade);
			}
		}
		
		
	}
	
	/**
	 * Notify `worker` has completed `task`
	 */
	public void complete (String task, String worker) {
		// Step 1: Remove worker from top worker set of the task
		SortedList topws = task2topws.get(task);
		if (topws != null) {
			topws.remove(worker);
		}
		
		// Step 2: Remove task from reverse task set of the worker
		List<String> rtopts = worker2rtopts.get(worker);
		if (rtopts!=null)
			rtopts.remove(task); 
	}
	
	public List<String> getTopKWorker (String task, int k) {
		List<String> topKWorkers = new ArrayList<String> ();
		SortedList topws = task2topws.get(task);
		for (int i = 0; i < topws.length(); i ++) {
			String worker = topws.getElement(i);
			if (topKWorkers.size() == k) break;
			topKWorkers.add(worker);
		}
		return topKWorkers;
	}

	
	public Estimator getEstimator() {
		return est;
	}
	
	protected void updateWorker2Rtopts(String task, Map<String, Boolean> upgrade) {
		for (String worker : upgrade.keySet()) {
			boolean hasUpgrade = upgrade.get(worker);
			List<String> rtopts = worker2rtopts.get(worker);
			if (rtopts == null) {
				rtopts = new ArrayList<String> ();
				worker2rtopts.put(worker, rtopts);
			}
			if (hasUpgrade) { // if worker is top-k worker, then add her. 
				if (rtopts.contains(task)) {
					//System.out.println("!!!!!!");
					continue;
				}
				rtopts.add(task);
			} else {
				// remove top workers
				rtopts.remove(task);
			}
			
		}
	}
	
	private Map<String, String> updateGlobalAssign (String reqWorker, List<String> rtopts) {
		Map<String, String> globalAsg = new HashMap<String, String> ();
		if (rtopts.isEmpty()) return globalAsg; // no reverse top tasks
		// The Greedy algorithm
		Set<String> candTasks = new HashSet<String> ();
		for (String task : rtopts) {
			if (accCtr.isGloballyCompleted(task)) continue;
			candTasks.add(task);
		}
		
		Set<String> selectedWorkers = new HashSet<String> ();
		Set<String> decidedTasks = new HashSet<String> ();
		while (decidedTasks.size() < candTasks.size()) { 
			// all workers have been assigned. 
			String maxtask = null; // Pick a max task
			Set<String> maxworkers = null; // with a set of max workers
			double maxscore = -100; // with a max-score
			for (String task : rtopts) {
				if (accCtr.isGloballyCompleted(task)) continue; // already completed
				if (decidedTasks.contains(task)) continue; // already decided
				List<String> topKWorkers = this.getTopKWorker(task, asgSize);
				List<String> asgedWorkers = accCtr.getAssignedWorkers(task);
				int kprime = asgSize - asgedWorkers.size();
				Set<String> selworkers = new HashSet<String> ();
				for (String worker : topKWorkers) {
					if (selectedWorkers.contains(worker)) {
						continue;
					}
					selworkers.add(worker);
					if (selworkers.size() == kprime) break;
				}
				if (selworkers.isEmpty()) {
					decidedTasks.add(task); // no more worker to be assigned
					continue;
				}
				double oldscore = 0;
				for (String asgedWorker : asgedWorkers) {
					double s = est.getAccuracy(task, asgedWorker);
					oldscore += s;
				}
				double newscore = 0;
				for (String selworker : selworkers) {
					double s = est.getAccuracy (task, selworker);
					newscore += s;
				}
				double score = newscore + oldscore;
				if (score > maxscore) {
					maxscore = score;
					maxworkers = selworkers;
					maxtask = task;
				}
			}
			if (maxtask != null) {
				decidedTasks.add(maxtask);
				boolean hasReq = false;
				for (String maxworker : maxworkers) {
					globalAsg.put(maxworker, maxtask);
					selectedWorkers.add(maxworker);
					if (maxworker.equals(reqWorker)) {
						hasReq = true;
					}
				}
				if (hasReq) break;
			}
		}
//		if (reqWorker.endsWith("FFN")) {
//			System.out.println("\t" + reqWorker + "\t" + globalAsg.get(reqWorker) + 
//					"\t" + rtopts.size());
//		}
		//System.out.println ("Assign overhead: " + candTasks.size() + "\t" + round + "\t" + selectedWorkers.size());
		return globalAsg;
	}
}
