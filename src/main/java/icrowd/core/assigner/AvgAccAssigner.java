package icrowd.core.assigner;

import icrowd.core.estimate.Estimator;
import icrowd.core.estimate.MeanEstimator;
import icrowd.utils.*;

import java.util.*;

/**
 * This task assigner considers an average accuracy for each worker, 
 *   and assigns each task to its top workers with higher accuracies. 
 * @author Ju Fan
 *
 */
public class AvgAccAssigner extends TaskAssigner {
	SortedList topWorkers = null; 
	MeanEstimator est = null;
	
	public AvgAccAssigner () {
		topWorkers = new SortedList (asgSize);
		est = new MeanEstimator ();
	}

	public void submitQTaskAnswer(String worker, 
			List<String> qtasks, List<Double> qanswers) {
		if (!est.containsWorker (worker)) {
			est.initAccuracy(null, worker);
			// initialize accuracy estimation for worker
		}	
		Map<String, Map<String, Double>> task2wperfs = 
				new HashMap<String, Map<String, Double>> ();
		for (int i = 0; i < qtasks.size(); i ++) {
			String qtask = qtasks.get(i);
			double qans = qanswers.get(i);
			Map<String, Double> wperfs = new HashMap<String, Double> ();
			wperfs.put(worker, qans); // the performance
			task2wperfs.put(qtask, wperfs);
		}
		
		est.updateOnPerfs(task2wperfs);
		
		topWorkers.update (worker, est.getAccuracy(null, worker));
		// Update the top worker list based on the initial/new estimation
	}

	public String assignTask (String worker) {
		List<ScoredItem> sortedTasks = new ArrayList<ScoredItem> ();
		List<String> candTasks = accCtr.getCandTasks (worker);
		if (candTasks == null) candTasks = mTasks;  
		for (String task : candTasks) {
			if (!accCtr.isValid (worker, task)) continue;
			List<String> asgWs = accCtr.getAssignedWorkers (task);
			int quota = asgSize - asgWs.size();
			// the number of workers still to be assigned with the task
			int rank = 0;
			Double accuracy = null;
			for (int i = 0;  i < topWorkers.length(); i ++) {
				String w = topWorkers.getElement (i);
				if (!accCtr.isValid(w, task)) continue;
				if (rank >= quota) break; // have found enough workers
				rank ++; // find a worker who can answer the task
				if (w.equals(worker)) { 
					accuracy = topWorkers.getScore (i);
					break; // Found the worker
				}
			}
			if (accuracy != null) { // the `worker` is within top-k'
				sortedTasks.add(new ScoredItem (task, accuracy));
			}
		}
		Collections.sort (sortedTasks);
		if (!sortedTasks.isEmpty()) {
			String asgTask = (String) sortedTasks.get(0).getObj(); 
			return asgTask;
		}
		return null;
	}

	/**
	 * AvgAccAssigner does not need to update assignment status
	 * 		when a task becomes globally completed. 
	 */
	public void globallyComplete (String task, List<String> asgWs, 
			List<Double> asgAs) { }

	public Estimator getEstimator() {
		return est;
	}

	public void complete(String task, String worker) {	}
	
}
