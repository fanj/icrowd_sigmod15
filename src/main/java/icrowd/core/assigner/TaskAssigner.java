package icrowd.core.assigner;

import icrowd.core.AccessControl;
import icrowd.core.estimate.Estimator;

import java.util.List;

/**
 * The generic interface for task assignment
 * @author Ju Fan
 *
 */
public abstract class TaskAssigner {
	protected List<String> mTasks;
	protected int asgSize;
	protected AccessControl accCtr;
	
	public void init(List<String> tasks, int assignSize, 
			AccessControl accessControl) {
		mTasks = tasks;
		asgSize = assignSize;
		accCtr = accessControl;
	}

	/**
	 * Update assignment status due to the submission of q-tasks
	 * @param worker: the worker completed the q-task
	 * @param qtasks: the q-tasks
	 * @param qanswers: the answer of the q-task
	 */
	public abstract void submitQTaskAnswer(String worker, 
			List<String> qtasks, List<Double> qanswers);

	/**
	 * Assign the worker a task to complete
	 * @param worker: the requesting worker
	 * @return
	 */
	public abstract String assignTask(String worker);

	/**
	 * Update assignment status 
	 *            due to a task has been globally completed
	 * @param task: the globally completed task
	 * @param asgWs: the list of workers completing the task
	 */
	public abstract void globallyComplete(String task, List<String> asgWs, 
			List<Double> asgAs) ;

	/**
	 * Return Estimator used in the task assigner
	 * @return: the estimator (null if the assigner does not use estimator) 
	 */
	public abstract Estimator getEstimator();

	public abstract void complete(String task, String worker);

}
