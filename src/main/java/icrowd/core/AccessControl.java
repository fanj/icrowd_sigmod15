package icrowd.core;

import java.util.*;

/**
 * This class controls which workers can access which tasks. 
 * Basically, there are several criteria: 
 *   1) A task can be assigned to a worker at most one time. 
 *   2) If a task has been globally completed according to assignment size k, 
 *         the task cannot be assigned to other workers
 *   3) If a task has been taken as warm-up for a worker, it cannot be assigned to the worker again. 
 * @author Ju Fan
 *
 */

public class AccessControl {
	final int STATE_COMPLETED = 1;
	final int STATE_ONGOING = 0;
	
	protected Map<String, Integer> task2status = 
			new HashMap<String, Integer> (); // all tasks
	Map<String, List<String>> worker2cands = null; 
	// Because we may not collect worker's answers on all tasks. 
	
	AsgMatrix asg = null;
	AsgMatrix warmup = null;

	/**
	 * @param tasks: all the tasks to be assigned
	 * @param workerCandidateTasks: candidate tasks for each worker -- For each worker, we may not collect her answers of all the tasks. 
	 *                              If this parameter is null, candidate tasks of each worker will be all the tasks.  
	 * @param asgSize: the assignment size per task
	 */
	public AccessControl(List<String> tasks,
			Map<String, List<String>> workerCandidateTasks, int asgSize, 
			AsgMatrix assign) {
		for (String task : tasks) {
			task2status.put(task, STATE_ONGOING);
			// all tasks are marked as `ongoing`
		}
		
		worker2cands = workerCandidateTasks;
		
		asg = assign;
		warmup = new AsgMatrix ();
	}
	
	public boolean isValid (String worker, String task) {
		if (!task2status.containsKey(task)) {
			return false;
		}
		if (task2status.get(task) == STATE_COMPLETED) {
			return false; // the task has been globally completed
		}
		List<String> assigned = asg.getAssignedTasks(worker);
		if (assigned.contains(task)) {
			return false; // the task is already assigned to the worker
		}
		if (warmup.containsWorker(worker)) {
			if (warmup.getAssignedTasks(worker).contains(task)) {
				return false; // the task has been taken as warm-up to this worker
			}
		}
		// If we do not collect answers of all tasks for a worker, 
		//    we need to check if the worker has answered this task. 
		if (worker2cands != null) {
			List<String> cands = worker2cands.get(worker);
			if (cands == null) return false;
			if (cands.contains(task))
				return true;
			else {
				return false;
			}
		}		
		return true;
	}
	
	/**
	 * Examine if the task is globally completed. 
	 * @param task: a specific task
	 * @return a boolean value indicating completeness. 
	 */
	public boolean isGloballyCompleted (String task) {
		int status = task2status.get(task);
		if (status == this.STATE_COMPLETED) return true;
		return false;
	}
	
	/**
	 * Mark a task as globally completed. 
	 * @param task: a specific task
	 */
	public void globallyComplete (String task) {
		task2status.put(task, STATE_COMPLETED);
	}

	/**
	 * Set warm-up tasks as well as their answers of a worker
	 * @param worker: a worker completing warm-up tasks
	 * @param qtask: the warm-up task, aka qualification task
	 * @param qans: the answer of the warm-up task
	 */
	public void addWarmup(String worker, String qtask, double qans) {
		this.warmup.put(qtask, worker, qans);
	}

	/**
	 * Get candidate tasks of a specific worker
	 * @param worker
	 * @return candidate tasks or "null" (indicating any task would be ok). 
	 */
	public List<String> getCandTasks(String worker) {
		if (worker2cands == null) {
			return null; 
		}
		return worker2cands.get(worker);
	}

	public List<String> getAssignedWorkers(String task) {
		return asg.getAssignedWorkers(task);
	}

}
