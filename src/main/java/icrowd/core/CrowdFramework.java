package icrowd.core;

import icrowd.core.aggregator.*;
import icrowd.core.assigner.*;
import icrowd.core.warmup.WarmUp;

import java.util.*;

/**
 * This is the basic framework of Crowdsourcing, including
 *   1) Task assignment, and 
 *   2) Worker answer aggregation. 
 * @author Ju Fan
 *
 */
public class CrowdFramework {
	protected List<String> mTasks; // all tasks to be assigned
	public AsgMatrix asg = null; // the worker-task assignment
	protected AccessControl accCtr; // used for access-control
	int asgSize = 0;
	
	protected TaskAssigner assigner; // used for task assignment
	protected AnswerAggregator aggregator; // used for answer aggregation
	protected WarmUp warmup; //used for warm-up
	
	/**
	 * The constructor of the Crowdsourcing framework
	 * @param tasks: the tasks to be assigned
	 * @param assignSize: the size of assignment per task
	 * @param worker2cands: the task constraint of each worker,
	 *                      should be null if there is no constraints
	 * @throws Exception
	 */
	public CrowdFramework (List<String> tasks, int assignSize, 
			Map<String, List<String>> worker2cands) throws Exception {
		asg = new AsgMatrix (); // the assignment matrix
		mTasks = tasks; 
		asgSize = assignSize;
		accCtr = new AccessControl (tasks, worker2cands, asgSize, asg);
		// Initialize a controller for access
	}
	
	public void initTaskAssigner (TaskAssigner taskAssigner) {
		assigner = taskAssigner;
		assigner.init (mTasks, asgSize, accCtr); 
		// initialize the task assigner
	}
	
	public void initAnswerAggregator (AnswerAggregator answerAggregator) {
		aggregator = answerAggregator;
	}
	
	public void initWarmUp (WarmUp warmUp) {
		warmup = warmUp;
	}
	
	/**
	 * Verify if the worker is new to the system
	 */
	public boolean containsWorker(String worker) {
		return asg.containsWorker(worker);
	}
	
	/**
	 * If the worker is new, these functions are called for warm-up.
	 */
	public List<String> assignQTasks (String worker) {
		List<String> qtasks = warmup.assignQTasks (worker);
		return qtasks;
	}
	
	public void submitQTaskAnswers (String worker, 
			List<String> qtasks, List<Double> qanswers) {
		for (int i = 0; i < qtasks.size(); i ++) {
			String qtask = qtasks.get(i);
			double qanswer = qanswers.get(i);
			accCtr.addWarmup(worker, qtask, qanswer); 
			// notify access controller about the q-task
		}
		assigner.submitQTaskAnswer (worker, qtasks, qanswers);
		// update task assigner by qualifications
	}
	
	
	/**
	 * Assign an `appropriate` task to a requesting worker
	 */
	public String assignTask(String worker) throws Exception {
		String task = assigner.assignTask (worker);
		return task;
	}
	
	/**
	 * Submit the answer of worker on a task
	 */
	public void submitAnswer(String task, String worker, double answer) {
		asg.put (task, worker, answer); // add it to assignment
		assigner.complete(task, worker);
		List<String> asgWs = asg.getAssignedWorkers(task);
		if (asgWs.size() == asgSize) { 
			List<Double> asgAs = new ArrayList<Double> ();
			for (String asgW : asgWs) {
				double ans = asg.getBenefit(task, asgW);
				asgAs.add (ans);
			}
			accCtr.globallyComplete(task);
			assigner.globallyComplete (task, asgWs, asgAs);
		}
	}

	/**
	 * Aggregate workers' answers to final results
	 */
	public Map<String, Integer> aggregateAnswers() throws Exception {
		Map<String, Integer> task2rst = 
				aggregator.aggregateAnswers (mTasks, asg, 
						assigner.getEstimator ());
		return task2rst;
	}
	
	public AsgMatrix getWorkerTaskAssignment () {
		return this.asg;
	}
}
