/**
 * A Dropped Class
 */


//package icrowd.core.warmup;
//
//import icrowd.utils.ScoredItem;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//public class WarmUpCategory implements WarmUp{
//	Map<String, String> task2cat = null;
//	Map<String, Map<String, Double>> worker2goldAnswers = null;
//	int qnum = 0;
//	public WarmUpCategory (Map<String, String> task2category, 
//			Map<String, Map<String, Double>> worker2golds, 
//			int qtaskNum) {
//		task2cat = task2category;
//		worker2goldAnswers = worker2golds;
//		qnum = qtaskNum;
//	}
//	
//	Map<String, Integer> qtask2occ = new HashMap<String, Integer>();
//	int QTASK_LIMIT = 5;
//	
//	public List<String> assignQTasks(String worker) {			
//		Map<String, Double> goldAnswers = worker2goldAnswers.get (worker);
//		// Step 1: Grouping tasks by category
//		Map<String, List<String>> cat2tasks = new HashMap<String, List<String>>();
//		for (String task : goldAnswers.keySet()) {
//			String cat = task2cat.get (task);
//			updateCategoryTaskList(cat2tasks, cat, task);
//		}
//		// Step 2: Select candidates from each category
//		Map<String, List<String>> qeCatToTasks = new HashMap<String, List<String>>();
//		Map<String, Double> qeCatToCorrect = new HashMap<String, Double>();
//		for (String cat : cat2tasks.keySet()) {
//			List<String> tasks = cat2tasks.get(cat);
//			for (int i = 0; i < tasks.size(); i++) {
//				String task = tasks.get(i);
//				Integer occ = qtask2occ.get(task);
//				if (occ == null || occ <= QTASK_LIMIT) {
//					updateCategoryTaskList(qeCatToTasks, cat, task);
//					double benefit = goldAnswers.get(task);
//					updateCategoryScore(qeCatToCorrect, cat, benefit);
//				}
//			}
//		}
//		// Step 3: Choose the more accurate categories
//		List<ScoredItem> qelist = new ArrayList<ScoredItem>();
//		for (String cat : qeCatToTasks.keySet()) {
//			Double qeCorrect = qeCatToCorrect.get(cat);
//			if (qeCorrect == null)
//				qeCorrect = 0.0;
//			int qeNum = qeCatToTasks.get(cat).size();
//			qeCorrect /= qeNum;
//			qelist.add(new ScoredItem(cat, qeCorrect));
//		}
//		Collections.sort(qelist);
//		Map<String, Double> selectedQTasks = new HashMap<String, Double>();
//		if (qelist.size() == 0) {
//			return new ArrayList<String> ();
//		}
//		int catnum = qnum / qelist.size();
//
//		for (ScoredItem catitem : qelist) {
//			String cat = (String) catitem.getObj();
//			double catacc = catitem.getScore();
//			int cornum = (int) Math.round(catnum * catacc);
//			int wrgnum = catnum - cornum;
//			List<String> qeTasks = qeCatToTasks.get(cat);
//			List<String> cortasks = new ArrayList<String>();
//			List<String> wrgtasks = new ArrayList<String>();
//			List<String> cattasks = new ArrayList<String>();
//			boolean glbsample = true;
//			for (String qetask : qeTasks) {
//				double goldans = goldAnswers.get(qetask);
//				if (glbsample) {
//					if (goldans == 1 && cortasks.size() < cornum) {
//						cortasks.add(qetask);
//						selectedQTasks.put(qetask, goldans);
//					} else if (goldans == 0 && wrgtasks.size() < wrgnum) {
//						wrgtasks.add(qetask);
//						selectedQTasks.put(qetask, goldans);
//					}
//				} else {
//					if (cattasks.size() < catnum) {
//						selectedQTasks.put (qetask, goldans);
//						cattasks.add(qetask);
//					} else {
//						break;
//					}
//				}
//			}
//			
//			for (String qeTask : qeTasks) {
//				Integer occ = qtask2occ.get(qeTask);
//				if (occ == null)
//					occ = 0;
//				occ++;
//				qtask2occ.put(qeTask, occ);
//			}
//		}
//		List<String> qtasklist = new ArrayList<String> ();
//		for (String qtask : selectedQTasks.keySet()) {
//			qtasklist.add(qtask);
//		}
//		return qtasklist;
//	}
//	
//	private static void updateCategoryTaskList(
//			Map<String, List<String>> cat2tasks, String cat, String task) {
//		List<String> tasks = cat2tasks.get(cat);
//		if (tasks == null)
//			tasks = new ArrayList<String>();
//		tasks.add(task);
//		cat2tasks.put(cat, tasks);
//	}
//
//	private static void updateCategoryScore(Map<String, Double> cat2score,
//			String cat, double s) {
//		Double score = cat2score.get(cat);
//		if (score == null)
//			score = 0.0;
//		score += s;
//		cat2score.put(cat, score);
//	}
//}
