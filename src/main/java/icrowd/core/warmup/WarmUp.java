package icrowd.core.warmup;

import java.util.*;

/**
 * This class is used for selecting qualification tasks, 
 *   which is for warm-up that addresses the cold-start problem. 
 * @author Ju Fan
 *
 */
public interface WarmUp {
	public abstract List<String> assignQTasks(String worker);
}
