package icrowd.core.warmup;

import java.util.*;
import java.io.*;

public class WarmupQualification implements WarmUp {

	Map<String, List<String>> worker2qualification = 
			new HashMap<String, List<String>> ();
	
	public WarmupQualification(String qualiFilename) throws Exception {
		BufferedReader r = new BufferedReader (new FileReader (qualiFilename));
		String line = r.readLine();
		while (line != null) {
			String tmps[] = line.split("\t");
			if (tmps.length == 2) {
				String worker = tmps[0];
				String taskStr = tmps[1];
				String[] tasks = taskStr.split(" ");
				List<String> tasklist = new ArrayList<String> ();
				for (String task : tasks) {
					tasklist.add(task);
				}
				worker2qualification.put(worker, tasklist);
			}
			line = r.readLine();
		}
		r.close();
	}

	public List<String> assignQTasks(String worker) {
		if (this.worker2qualification.containsKey(worker)) {
			return worker2qualification.get(worker);
		} else {
			return new ArrayList<String> ();
		}
	}

}
