package icrowd.core.estimate;

import icrowd.backends.jgibblda.LDA;
import icrowd.test.eval.dataset.DatasetLoader;

import java.io.*;
import java.nio.file.Files;
import java.util.*;

import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleWeightedGraph;

/**
 * This class is used to build the necessary files for the graph-based estimator
 * @author Ju Fan
 *
 */
public class GraphEstimatorBuilder {
	
	/**********************************************************
	 * Functions related to similarity computation
	 **********************************************************/
	public static final String SIM_LDA = "LDA";
	public static final String SIM_JACCARD = "JACCARD";
	public static final String SIM_COSINE = "COSINE";
	
	
	/**
	 * Prepare the PPR file, which is used for online graph-based estimation
	 * @param simFunc
	 * @param simThreshold
	 * @param miu
	 * @param outfolder
	 * @throws Exception
	 */
	public static void preparePPR ( String taskIdFilename, String taskTextFilename, 
			String simFunc, double simThreshold, double miu, 
			String outFilename) throws Exception {
		String tempFolder = "temp/";
		
		File file = new File (outFilename);
		if (file.exists()) return; // the file is already there
		String simfile = null;
		if (simFunc.equals(SIM_LDA)) {
			File taskTextFile = new File (taskTextFilename);
			File tmpTaskTextFile = new File (tempFolder + "task_text");
			if (tmpTaskTextFile.exists()) tmpTaskTextFile.delete();
			Files.copy(taskTextFile.toPath(), tmpTaskTextFile.toPath());
			String[] ldaArgs = {
					"-est", "-alpha", "0.5", "-beta", "0.1", "-ntopics", "10",
					"-niters", "1000", "-savestep", "1000", "-twords", "20", 
					"-dfile", "task_text", "-dir", tempFolder,
			};
			LDA.main(ldaArgs); // run the LDA back-end
			
			String ldaFilename = tempFolder + "model-final.theta";
			simfile = tempFolder + "lda_sim.out";
			computeLDASim (taskIdFilename, ldaFilename, simfile, simThreshold);
		} else if (simFunc.equals(SIM_JACCARD)) {
			simfile = tempFolder + "jaccard_sim.out";
			computeJaccardSim (taskIdFilename, taskTextFilename, simfile, simThreshold);
		} else if (simFunc.equals(SIM_COSINE)) {
			simfile = tempFolder + "cosine_sim.out";
			computeCosineSim (taskIdFilename, taskTextFilename, simfile, simThreshold);
		}
		if (simfile != null)
			simToPPVs (simfile, outFilename, miu, false);
	}
	
	private static void computeCosineSim(String idInfile, String textInfile,
			String simfile, double simThreshold) throws Exception {
		List<String> tasks = new ArrayList<String> ();
		BufferedReader r = new BufferedReader (new FileReader (idInfile));
		String line = r.readLine();
		while (line != null) {
			line = line.trim();
			if (!line.isEmpty()) {
				tasks.add(line);
			}
			line = r.readLine();
		}
		r.close();
		
		List<Map<String, Double>> task2token2TF = new ArrayList<Map<String, Double>> ();
		Map<String, Double> token2DF = new HashMap<String, Double> ();
		
		r = new BufferedReader (new FileReader (textInfile));
		line = r.readLine();
		line = r.readLine();
		while (line != null) {
			String[] tokens = line.split(" ");
			Map<String, Double> token2TF = new HashMap<String, Double> ();
			for (String token : tokens) {
				Double tf = token2TF.get(token);
				if (tf == null) tf = 0.0;
				tf ++;
				token2TF.put(token, tf);
				
				Double df = token2DF.get(token);
				if (df == null) df = 0.0;
				df ++;
				token2DF.put(token, df);
			}
			task2token2TF.add(token2TF);
			line = r.readLine();
		}
		r.close();
		
		BufferedWriter w = new BufferedWriter (new FileWriter (simfile));
		for (int i = 0; i < tasks.size(); i ++) {
			//System.out.println ("Progress: " + i + " / " + tasks.size());
			String t1 = tasks.get(i);
			Map<String, Double> token2TF1 = task2token2TF.get(i);
			for (int j = i + 1; j < tasks.size(); j ++) {
				String t2 = tasks.get(j);
				Map<String, Double> token2TF2 = task2token2TF.get(j);
				double sim = 0.0;
				double norm1 = 0.0;
				double norm2 = 0.0;
				
				for (String token : token2TF1.keySet()) {
					double tf1 = token2TF1.get(token);
					double df = token2DF.get(token);
					Double tf2 = token2TF2.get(token);
					if (tf2 == null) tf2 = 0.0;
					double w1 = tf1 * Math.log(1 / (df + 1));
					double w2 = tf2 * Math.log(1 / (df + 1));
					sim += w1 * w2;
					norm1 += w1 * w1;
					norm2 += w2 * w2;
				}
				
				for (String token : token2TF2.keySet()) {
					Double tf1 = token2TF1.get(token);
					if (tf1 == null) tf1 = 0.0;
					double df = token2DF.get(token);
					double tf2 = token2TF2.get(token);
					double w1 = tf1 * Math.log(1 / (df + 1));
					double w2 = tf2 * Math.log(1 / (df + 1));
					sim += w1 * w2;
					norm1 += w1 * w1;
					norm2 += w2 * w2;
				}
				sim = sim / (Math.sqrt(norm1 * norm2));
				if (sim >= simThreshold) {
					w.write(t1 + "\t" + t2 + "\t" + sim);
					w.newLine();
				}
				
			}
		}
		w.close();
		
	}

	private static void computeJaccardSim(String idInfile, String textInfile,
			String simfile, double simThreshold) throws Exception {
		List<String> tasks = new ArrayList<String> ();
		BufferedReader r = new BufferedReader (new FileReader (idInfile));
		String line = r.readLine();
		while (line != null) {
			line = line.trim();
			if (!line.isEmpty()) {
				tasks.add(line);
			}
			line = r.readLine();
		}
		r.close();
		
		List<Set<String>> task2tokens = new ArrayList<Set<String>> ();
		r = new BufferedReader (new FileReader (textInfile));
		line = r.readLine();
		line = r.readLine();
		while (line != null) {
			String[] tokens = line.split(" ");
			Set<String> tokenSet = new HashSet<String> ();
			for (String token : tokens) {
				tokenSet.add(token);
			}
			task2tokens.add(tokenSet);
			line = r.readLine();
		}
		r.close();
		
		BufferedWriter w = new BufferedWriter (new FileWriter (simfile));
		for (int i = 0; i < tasks.size(); i ++) {
			//System.out.println ("Progress: " + i + " / " + tasks.size());
			String t1 = tasks.get(i);
			Set<String> tokenset1 = task2tokens.get(i);
			for (int j = i + 1; j < tasks.size(); j ++) {
				String t2 = tasks.get(j);
				Set<String> tokenset2 = task2tokens.get(j);
				double sim = 0.0;
				for (String token : tokenset1) {
					if (tokenset2.contains(token)) sim ++;
				}
				sim = sim / (tokenset1.size() + tokenset2.size() - sim);
				if (sim >= simThreshold) {
					w.write(t1 + "\t" + t2 + "\t" + sim);
					w.newLine();
				}
				
			}
		}
		w.close();
	}

	public static void computeLDASim (String idInfile, String ldafile, 
			String outfile, double threshold) throws Exception {
		List<String> tasks = new ArrayList<String> ();
		BufferedReader r = new BufferedReader (new FileReader (idInfile));
		String line = r.readLine();
		while (line != null) {
			line = line.trim();
			if (!line.isEmpty()) {
				tasks.add(line);
			}
			line = r.readLine();
		}
		r.close();
		
		List<List<Double>> task2scores = new ArrayList<List<Double>> ();
		r = new BufferedReader (new FileReader (ldafile));
		line = r.readLine();
		while (line != null) {
			line = line.trim();
			if (!line.isEmpty()) {
				String tmps[] = line.split(" ");
				List<Double> scores = new ArrayList<Double> ();
				for (String tmp : tmps) {
					scores.add(Double.parseDouble(tmp));
				}
				task2scores.add(scores);
			}
			line = r.readLine();
		}
		r.close();
		BufferedWriter w = new BufferedWriter (new FileWriter (outfile));
		for (int i = 0; i < tasks.size(); i ++) {
			//System.out.println ("Progress: " + i + " / " + tasks.size());
			String t1 = tasks.get(i);
			List<Double> sc1 = task2scores.get(i);
			for (int j = i + 1; j < tasks.size(); j ++) {
				String t2 = tasks.get(j);
				List<Double> sc2 = task2scores.get(j);
				double sim = 0.0;
				double n1 = 0.0;
				double n2 = 0.0;
				for (int k = 0; k < sc1.size(); k ++) {
					sim += sc1.get(k) * sc2.get(k);
					n1 += sc1.get(k) * sc1.get(k);
					n2 += sc2.get(k) * sc2.get(k);
				}
				sim /= Math.sqrt(n1 * n2);
				if (sim >= threshold) {
					w.write(t1 + "\t" + t2 + "\t" + sim);
					w.newLine();
				}
				
			}
		}
		w.close();
	}
	
	
	/**********************************************************
	 * Functions related to Personalized PageRank Computation
	 **********************************************************/

	
	/**
	 * This function compute PP-Vector for each task given inter-task similarities
	 * @param simInfile
	 */
	public static void simToPPVs (String simInfile, String pprOutfile, double miu, boolean isRadical) throws Exception {
		/**
		 * Step 1: Build a graph;
		 */
		SimpleWeightedGraph<String, DefaultWeightedEdge> graph = 
				new SimpleWeightedGraph<String, DefaultWeightedEdge> (DefaultWeightedEdge.class);
		BufferedReader r = new BufferedReader (new FileReader (simInfile));
		String line = r.readLine();
		while (line != null) {
			line = line.trim();
			String tmps[] = line.split("\t");
			String task1 = tmps[0];
			String task2 = tmps[1];
			double sim = Double.parseDouble(tmps[2]);
			if (!graph.containsVertex(task1)) graph.addVertex(task1);
			if (!graph.containsVertex(task2)) graph.addVertex(task2);
			DefaultWeightedEdge e = graph.addEdge(task1, task2);
			graph.setEdgeWeight(e, sim);
			line = r.readLine();
		}
		r.close();
		
		/**
		 * Step 2: Normalize the graph;
		 */
		Map<String, Double> vertexSum = new HashMap<String, Double>();
		for (String v : graph.vertexSet()) {
			double sum = 0.0;
			Set<DefaultWeightedEdge> edges = graph.edgesOf(v);
			for (DefaultWeightedEdge edge : edges) {
				sum += graph.getEdgeWeight(edge);
			}
			vertexSum.put(v, sum);
		}

		//for (DefaultWeightedEdge e : graph.edgeSet()) {
			//String source = graph.getEdgeSource(e);
			//double sourceSum = vertexSum.get(source);
			//String target = graph.getEdgeTarget(e);
			//double targetSum = vertexSum.get(target);

			//double weight = graph.getEdgeWeight(e);
			//weight /= (Math.sqrt(sourceSum) * Math.sqrt(targetSum));
			//graph.setEdgeWeight(e, weight);
		//}
		
		/**
		 * Step 3: Compute PP-Vector for each vertex
		 */
		
		Map<String, Map<String, Double>> PPVs = 
				new HashMap<String, Map<String, Double>>();
		int count = 0;
		for (String vertex : graph.vertexSet()) {
			Map<String, Double> initPpr = new HashMap<String, Double>();
			// Initialization
			for (String v : graph.vertexSet()) {
				if (v.equals(vertex))
					initPpr.put(v, 1.0);
				else
					initPpr.put(v, 0.0);
			}
			Set<String> seeds = new HashSet<String> ();
			seeds.add(vertex);
			Map<String, Double> ppr = doRunPPR(seeds, initPpr, graph, miu, isRadical);
			PPVs.put(vertex, ppr);
			if (count % 100 == 0) {
				System.out.println ("\t\t PPV Progress: " + count + "/" + graph.vertexSet().size());
				//break;
			}
			count ++;
			
		}
		
		BufferedWriter w = new BufferedWriter (new FileWriter (pprOutfile));
		for (String vertex : PPVs.keySet()) {
			w.write(vertex + "\t");
			Map<String, Double> ppv = PPVs.get(vertex);
			for (String v : ppv.keySet()) {
				w.write(v + ":" + ppv.get(v) + " ");
			}
			w.newLine();
		}
		w.close();
		
	}
	
	private static Map<String, Double> doRunPPR(Set<String> seeds, Map<String, Double> initPpr, 
			SimpleWeightedGraph<String, DefaultWeightedEdge> graph, double miu, boolean isRadical) {
		double lambda = 1 / (1 + miu);
		Map<String, Double> ppr = initPpr;
		// Iterations
		int maxIter = 1000;
		for (int iter = 0; iter < maxIter; iter++) {
			//System.out.println (iter);
			Map<String, Double> newPpr = new HashMap<String, Double>();
			for (String v : ppr.keySet()) {
				double score1 = (1 - lambda) * initPpr.get(v);
				Set<DefaultWeightedEdge> edges = graph.edgesOf(v);
				double score2 = 0.0;
				double totalWeight = 0.0;
				for (DefaultWeightedEdge edge : edges) {
					String neighbor = graph.getEdgeSource(edge);
					if (neighbor.equals(v)) neighbor = graph.getEdgeTarget(edge);
					double weight = graph.getEdgeWeight(edge);
					double neigoborScore = ppr.get(neighbor);
					score2 += weight * neigoborScore;
					totalWeight += weight;
				}
				if (score2 > 0.0)
					score2 /= totalWeight; 
				score2 *= lambda;
				double score = score1 + score2;
				if (isRadical && seeds.contains(v)) score = initPpr.get(v);
				newPpr.put(v, score);
			}

			double diff = 0.0;
			for (String v : ppr.keySet()) {
				double oldS = ppr.get(v);
				double newS = newPpr.get(v);
				diff += Math.abs(oldS - newS);
			}
			//System.out.println ("It #" + iter + "\t" + diff );
//			diff /= ppr.size();

			ppr = newPpr;
			
			if (diff < 0.000001) {
				//System.out.println ("break: " + iter);
				break;
			}
		}
		return ppr;
	}
	
	public static void main (String args[]) throws Exception {
		if (args.length != 6) {
			System.out.println ("Sample Usage: GraphEstimatorBuilder "
					+ "data/sample/ppv/task_id data/sample/ppv/task_text "
					+ "LDA 0.8 1.0 data/sample/ppv/lda_0.8_1.0");
			return;
		}
		String taskIdFilename = args[0]; // Task IDs
		String taskTextFilename = args[1]; // Task text
		String simFunc = args[2]; // similarity function: LDA, JACCARD, COSINE
		double simTh = Double.parseDouble(args[3]); // similarity threshold
		double alpha = Double.parseDouble(args[4]); // alpha
		
		String outFilename = args[5]; 
		
		preparePPR (taskIdFilename, taskTextFilename, 
				simFunc, simTh, alpha, outFilename);	
	}
}
