package icrowd.core.estimate;

public interface Estimator {	
	/**
	 * Initialize the accuracy of `worker` on `task`
	 * @param task
	 * @param worker
	 */
	public abstract void initAccuracy (String task, String worker);
	/**
	 * Get the `worker`'s accuracy on the `task`
	 * @param task
	 * @param worker
	 * @return
	 */
	public abstract Double getAccuracy(String task, String worker);	
	
	/**
	 * Examine if the estimator has record of `worker`
	 * @param worker
	 * @return
	 */
	public abstract boolean containsWorker(String worker);
}
