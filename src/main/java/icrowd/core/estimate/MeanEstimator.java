package icrowd.core.estimate;

import java.util.*;

public class MeanEstimator implements Estimator {

	Map<String, Integer> worker2num = new HashMap<String, Integer> ();
	Map<String, Double> worker2cor = new HashMap<String, Double> ();	
	
	public void updateOnPerfs (Map<String, Map<String, Double>> task2wperfs) {
		if (task2wperfs == null) return;
		for (String task : task2wperfs.keySet()) {
			Map<String, Double> wperfs = task2wperfs.get(task);
			for (String worker : wperfs.keySet()) {
				double perf = wperfs.get(worker);
				// Update assigned number
				Integer num = worker2num.get(worker); 
				num ++;
				worker2num.put(worker, num);
				
				// Update correct answered number
				Double cor = worker2cor.get(worker);
				cor += perf;
				worker2cor.put(worker, cor);
			}
		}
 	}

	public Double getAccuracy(String task, String worker) {
		double cor = worker2cor.get(worker);
		double num = worker2num.get(worker);
		double acc = (cor + 1) / (num + 2);
		return acc;
	}

	public void initAccuracy(String task, String worker) {
		if (!worker2num.containsKey(worker)) {
			worker2cor.put(worker, 0.0);
			worker2num.put(worker, 0);
		}
	}

	public boolean containsWorker(String worker) {
		return worker2num.containsKey(worker);
	}
}
