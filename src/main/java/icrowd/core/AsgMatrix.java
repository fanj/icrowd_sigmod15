package icrowd.core;


import java.text.DecimalFormat;
import java.util.*;

public class AsgMatrix {
	public Map<String, Map<String, Double>> worker2tasks; // store by workers
	Map<String, Map<String, Double>> worker2taskVars;
	
	public AsgMatrix () {
		worker2tasks = new HashMap<String, Map<String, Double>> ();
		worker2taskVars = new HashMap<String, Map<String, Double>> ();
	}
	
	public AsgMatrix clone () {
		AsgMatrix cloned = new AsgMatrix ();
		for (String worker : worker2tasks.keySet()) {
			for (String task : worker2tasks.get(worker).keySet()) {
				double benefit = worker2tasks.get(worker).get(task);
				cloned.put(task, worker, benefit);
			}
		}
		return cloned;
	}
	
	public List<String> getAssignedWorkers (String task) {
		List<String> workerlist = new ArrayList<String> ();
		for (String worker : worker2tasks.keySet()) {
			Map<String, Double> tasks = worker2tasks.get(worker);
			if (tasks.containsKey(task)) workerlist.add(worker);
		}
		return workerlist;
	}
	
	public Map<String, Double> getAssignedTaskScores (String worker) {
		return worker2tasks.get(worker);
	}
	
	public List<String> getAssignedTasks (String worker) {
		Map<String, Double> tasks = worker2tasks.get(worker);
		List<String> tasklist = new ArrayList<String> ();
		if (tasks != null) {
			for (String task : tasks.keySet()) {
				tasklist.add(task);
			}
		}
		return tasklist;
	}
	
	public void putVar (String task, String worker, double var) {
		Map<String, Double> taskVars = worker2taskVars.get(worker);
		if (taskVars == null) taskVars = new HashMap<String, Double> ();
		taskVars.put(task, var);
		worker2taskVars.put(worker, taskVars);
	}
	
	public void put (String task, String worker, double benefit) {
		Map<String, Double> tasks = worker2tasks.get(worker);
		if (tasks == null) tasks = new HashMap<String, Double> ();
		tasks.put(task, benefit);
		worker2tasks.put(worker, tasks);
	}
	
	public Double getBenefitVar (String task, String worker) {
		Map<String, Double> taskVars = worker2taskVars.get(worker);
		if (taskVars == null) return null;
		return taskVars.get(task);
	}
	
	public Double getBenefit (String task, String worker) {
		Map<String, Double> tasks = worker2tasks.get(worker);
		if (tasks == null) return null;
		if (!tasks.containsKey(task)) return null;
		double s = (double)tasks.get(task);
		DecimalFormat df = new DecimalFormat("#.####");
		double round_s = Double.parseDouble(df.format(s));
		return round_s;
	}
	
	public void addWorker (String worker) {
		worker2tasks.put(worker, new HashMap<String, Double> ());
	}
	
	public boolean containsWorker (String worker) {
		return worker2tasks.containsKey(worker);
	}
	
	public double additiveAgg () {
		double sum = 0.0;
		for (String worker : worker2tasks.keySet()) {
			Map<String, Double> tasks = worker2tasks.get(worker);
			for (String task : tasks.keySet()) {
				sum += tasks.get(task);
			}
		}
		return sum;
	}

	public String toString () {
		StringBuffer buffer = new StringBuffer ();
		for (String worker : worker2tasks.keySet()) {
			Map<String, Double> tasks = worker2tasks.get(worker);
			for (String task : tasks.keySet()) {
				double benefit = tasks.get(task);
				buffer.append(task + "\t" + worker + "\t" + benefit);
				buffer.append("\n");
			}
		}
		return buffer.toString();
	}
}
